Rails.application.routes.draw do

  devise_for :users, controllers: {
    sessions: 'sessions',
    registrations: 'registrations',
    passwords: 'passwords',
    confirmations: 'confirmations',
    omniauth_callbacks: 'omniauth_callbacks'
  }
  resources :users, only: [ :show ] do
    collection do
      get :navigation
      get :achievements
      get :edit_password
      patch :update_password
    end
  end

  get 'admin/dashboard'

  namespace :admin do
    get 'unconfirmed'
    get 'imageless'
    resources :users, only: [:index, :show, :update]
    match 'settings', to: 'settings#edit', via: :get
    match 'settings', to: 'settings#update', via: [:put, :patch]
  end

  resources :topics, only: [:show]

  resources :edits, only: [:edit, :update, :destroy] do
    member do
      patch :apply
      patch :discard
    end
  end

  resources :reports, only: [:new, :create]
  scope :admin do
    resources :reports, except: [:new, :create], as: 'admin_reports'
  end

  resources :votes, only: [:create, :show] do
    collection do
      match 'show/:id', to: 'votes#for_show', via: :get, as: :show
      match 'person/:id', to: 'votes#for_person', via: :get, as: :person
      match 'show/:id/mine', to: 'votes#mine_for_show', via: :get, as: :my_show
      match 'person/:id/mine', to: 'votes#mine_for_person', via: :get, as: :my_person
    end
  end

  resources :people do
    resources :books do
      member do
        post :rate
        get :rating
      end
    end
    member do
      post :favorite
      get :votes, to: 'votes#for_person'
      get 'votes/mine', to: 'votes#mine_for_person'
      get :shows
      get :episodes
      get :about
      post :rate
      get :rating
      get :edits
      get :merge, to: 'people#merge_form'
      post :merge
    end
  end

  resources :shows do
    resources :people do
      collection do
        get 'previous', to: 'people#previously_on_show'
        get 'other', to: 'people#new_to_show'
      end
      member do
        get :show, to: 'people#for_show'
      end
    end
    resources :sources do
      member do
        get :fetch
        post :generate
      end
    end
    resources :episodes do
      get :appearances, to: 'episodes#appearances'
      post :appearances, to: 'episodes#appear'
      delete :appearances, to: 'episodes#disappear'
      member do
        post :rate
        get :rating
        get :edits
        get :merge, to: 'episodes#merge_form'
        post :merge
      end
    end
    member do
      match 'votes', to: 'votes#for_show', via: :get
      match 'votes/mine', to: 'votes#mine_for_show', via: :get
      post :rate
      get :rating
      get :edits
    end
  end

  resources :notifications, only: [:index, :destroy] do
    collection do
      patch :read
    end
  end

  get 'search', to: 'application#search'

  get '/welcome', to: 'pages#welcome'
  get '/privacy', to: 'pages#privacy'
  get '/terms', to: 'pages#terms'
  get '/sitemap', to: 'pages#sitemap'

  match '/404', to: 'pages#not_found', via: :all
  match '/500', to: 'pages#internal_server_error', via: :all

  root 'pages#welcome'
end
