# List of available languages
I18n.available_locales = [:en, :sv]

# Map of locale and its name
LANGUAGES = {
  en: 'English',
  sv: 'Swedish'
}
