require 'google/cloud/storage'

# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://shocadb.com"

SitemapGenerator::Sitemap.adapter = SitemapGenerator::GoogleStorageAdapter.new(
  credentials: 'tmp/gcp.json',
  project_id: Rails.application.credentials.dig(:google_cloud, :project_id),
  bucket: Rails.application.credentials.dig(:google_cloud, :sitemap, :bucket)
)

SitemapGenerator::Sitemap.public_path = 'tmp/'
SitemapGenerator::Sitemap.ping_search_engines('https://shocadb.com/sitemap')

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end

  add people_path, priority: 0.7, changefreq: 'daily'
  Person.find_each do |person|
    add person_path(person), priority: 0.6, lastmod: person.updated_at
  end

  add shows_path, priority: 0.7, changefreq: 'daily'
  Show.find_each do |show|
    add show_path(show), priority: 0.6, lastmod: show.updated_at
    Person.find_each do |person|
      add show_person_path(show, person), priority: 0.9,
                                          lastmod: [person.updated_at, show.updated_at].max
    end
    show.episodes.each do |episode|
      add show_episode_path(show, episode), priority: 0.3, lastmod: episode.updated_at
    end
  end
end
