# frozen_string_literal: true

require 'application_system_test_case'

class VotesTest < ApplicationSystemTestCase
  setup do
    @person = create(:person)
    @show = create(:show)
  end

  test 'voting on person for show when signed out' do
    visit show_person_url(@show, @person)
    assert_selector 'h2', text: "Bring #{@person.name} to #{@show.title}"
    assert_selector '.btn', text: 'Vote now!'

    assert_difference 'Vote.count' do
      click_on 'Vote now'
    end

    assert_equal @person, Vote.last.person
    assert_equal @show, Vote.last.show
    assert_match(/\d+\.\d+\.\d+\.\d+/, Vote.last.ip_address)
    assert_equal nil, Vote.last.user

    assert_selector 'h2', text: "Bring #{@person.name} to #{@show.title}"
    assert_selector '.alert', text: 'Vote was successful.'
  end

  test 'voting on person for show when signed in' do
    user = create(:user)
    login_as user, scope: :user

    visit show_person_url(@show, @person)
    assert_selector 'h2', text: "Bring #{@person.name} to #{@show.title}"
    assert_selector '.btn', text: 'Vote now!'

    assert_difference 'Vote.count' do
      click_on 'Vote now'
    end

    assert_equal @person, Vote.last.person
    assert_equal @show, Vote.last.show
    assert_equal nil, Vote.last.ip_address
    assert_equal user, Vote.last.user

    assert_selector 'h2', text: "Bring #{@person.name} to #{@show.title}"
    assert_selector '.alert', text: 'Vote was successful.'
    assert_selector '.btn', text: 'Already voted'
  end

  test 'voting on person on show when signed out' do
    person = create(:person, name: 'My Person')
    visit show_url(@show)
    assert_selector '.card-header', text: 'MY VOTES'
    assert_selector '.card', text: 'You have not voted yet.'
    assert_selector '.card-header', text: 'NEW GUESTS'
    assert_selector '.card li.list-group-item', text: 'My Person'

    assert_difference 'Vote.count' do
      click_on 'Vote for My Person'
      assert_selector '.alert', text: 'You have no votes left.'
    end

    assert_equal person, Vote.last.person
    assert_equal @show, Vote.last.show
    assert_match(/\d+\.\d+\.\d+\.\d+/, Vote.last.ip_address)
    assert_equal nil, Vote.last.user
  end

  test 'voting on person on show when signed in' do
    person = create(:person, name: 'My Person')
    user = create(:user)
    login_as user, scope: :user

    visit show_url(@show)
    assert_selector '.card-header', text: 'MY VOTES'
    assert_selector '.card', text: 'You have not voted yet.'
    assert_selector '.card-header', text: 'NEW GUESTS'
    assert_selector '.card li.list-group-item', text: 'My Person'

    assert_difference 'Vote.count' do
      click_on 'Vote for My Person'
      assert_selector '.alert', text: 'You have 4 votes left.'
    end

    assert_equal person, Vote.last.person
    assert_equal @show, Vote.last.show
    assert_equal nil, Vote.last.ip_address
    assert_equal user, Vote.last.user
  end

  test 'voting on new person on show when signed out' do
    visit show_url(@show)
    assert_no_difference 'Vote.count' do
      assert_no_difference 'Person.count' do
        fill_in 'Guest name', with: 'Custom Person'
        click_on 'Vote for new person'
        assert_selector '.alert', text: 'You cannot create this person.'
      end
    end
  end

  test 'voting on new person on show when signed in' do
    user = create(:user)
    login_as user, scope: :user

    visit show_url(@show)
    assert_difference 'Vote.count' do
      assert_difference 'Person.count' do
        fill_in 'Guest name', with: 'Custom Person'
        click_on 'Vote for new person'
        assert_selector '.alert', text: 'You have 4 votes left.'
      end
    end

    assert_equal 'Custom Person', Vote.last.person.name
    assert_equal @show, Vote.last.show
    assert_equal nil, Vote.last.ip_address
    assert_equal user, Vote.last.user
  end

  test 'voting on new show on person when signed out' do
    visit person_url(@person)
    assert_no_difference 'Vote.count' do
      assert_no_difference 'Show.count' do
        fill_in 'Show name', with: 'Custom Show'
        click_on 'Vote for new show'
        assert_selector '.alert', text: 'You cannot create this show.'
      end
    end
  end

  test 'voting on new show on person when signed in' do
    user = create(:user)
    login_as user, scope: :user

    visit person_url(@person)
    assert_difference 'Vote.count' do
      assert_difference 'Show.count' do
        fill_in 'Show name', with: 'Custom Show'
        click_on 'Vote for new show'
        assert_selector '.alert', text: 'You have 4 votes left.'
      end
    end

    assert_equal 'Custom Show', Vote.last.show.title
    assert_equal @person, Vote.last.person
    assert_equal nil, Vote.last.ip_address
    assert_equal user, Vote.last.user
  end
end
