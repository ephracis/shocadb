# frozen_string_literal: true

require 'application_system_test_case'

class AdminTest < ApplicationSystemTestCase
  setup do
    create(:root_user)
  end

  test 'should not see admin link when signed out' do
    visit root_url
    assert_selector 'nav', text: 'Admin', count: 0
  end

  test 'should not see admin link when user' do
    login_as create(:user), scope: :user
    visit root_url
    assert_selector 'nav', text: 'Admin', count: 0
  end

  test 'should see admin link when admin' do
    login_as create(:admin_user), scope: :user
    visit root_url
    assert_selector 'nav', text: 'Admin', count: 1
  end

  test 'should see admin link when root' do
    login_as create(:user, roles: [Role.find_by(name: 'root')]), scope: :user
    visit root_url
    assert_selector 'nav', text: 'Admin', count: 1
  end
end
