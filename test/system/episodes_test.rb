# frozen_string_literal: true

require 'application_system_test_case'

class EpisodesTest < ApplicationSystemTestCase
  setup do
    @show = create(:show)
  end

  test 'visiting the index signed out' do
    visit show_url(@show)
    assert_selector '.card-header', text: 'EPISODES'
    assert_selector 'a', text: 'Add episode', count: 0
  end

  test 'visiting the index as user' do
    login_as create(:user), scope: :user
    visit show_url(@show)
    assert_selector '.card-header', text: 'EPISODES'
    assert_selector 'a', text: 'Add episode', count: 0
  end

  test 'visiting the index as admin' do
    login_as create(:admin_user), scope: :user
    visit show_url(@show)
    assert_selector '.card-header', text: 'EPISODES'
    assert_selector 'a', text: 'Add episode'
  end

  test 'creating an episode' do
    login_as create(:admin_user), scope: :user

    visit show_url(@show)
    click_on 'Add episode'

    fill_in 'Description', with: 'This is my test episode.'
    fill_in 'Title', with: 'Test Episode'
    fill_in 'Aliases', with: 'A Test Episode,'
    fill_in 'Topics', with: 'Test Topic,'
    click_on 'Save'

    assert_text 'The episode has been created'
    assert_equal 'Test Episode', Episode.last.title
    assert_equal Episode.last, Episode.find_by_alias('A Test Episode')
    assert_equal 'Test Topic', Episode.last.topics.first.name
  end

  test 'updating an episode' do
    episode = create(:episode, show: @show)
    login_as create(:admin_user), scope: :user

    visit show_url(@show)
    find('.dropdown a[type="button"]', match: :first).click
    find('.dropdown-menu a', text: 'Edit').click

    fill_in 'Slug', with: 'a-new-slug'
    fill_in 'Description', with: 'A new description.'
    click_on 'Save'
    episode.reload

    assert_text 'The episode has been updated'
    assert_equal 'A new description.', Episode.last.description
    assert_equal 'a-new-slug', Episode.last.slug
  end

  test 'destroying an episode' do
    create(:episode, show: @show)
    login_as create(:admin_user), scope: :user

    visit show_url(@show)
    find('.dropdown a[type="button"]', match: :first).click
    page.accept_confirm do
      find('.dropdown-menu a', text: 'Remove').click
    end

    assert_text 'The episode has been removed'
    assert_equal 0, @show.episodes.count
  end

  test 'add existing person to episode' do
    episode = create(:episode, show: @show, youtube: nil, spotify: nil)
    person = create(:person, name: 'Test Person')
    login_as create(:admin_user), scope: :user

    visit show_episode_url(@show, episode)
    fill_in 'Guest name', with: 'Test Person'
    click_on 'Add guest'

    assert_text 'Guest was added'
    assert_equal @show, person.shows.last
  end

  test 'add new person to episode' do
    episode = create(:episode, show: @show, youtube: nil, spotify: nil)
    login_as create(:admin_user), scope: :user

    visit show_episode_url(@show, episode)
    fill_in 'Guest name', with: 'Test Person'
    click_on 'Add guest'

    assert_text 'Guest was added'
    assert_equal @show, Person.last.shows.last
    assert_equal 'Test Person', Person.last.name
  end

  test 'marking episode as duplicate' do
    login_as create(:admin_user), scope: :user

    original = create(:episode, show: @show, title: 'Original Episode', description: nil)
    duplicate = create(:episode, show: @show, title: 'Duplicate Episode', description: 'A nice description.')

    visit show_url(@show)
    find("li[data-episode-id='#{duplicate.id}'] .dropdown a[type='button']", match: :first).click
    click_on 'Merge', match: :first

    assert_selector 'h1', text: 'Merge the episode'
    assert_selector 'h5', text: 'Duplicate Episode'
    assert_selector 'h2', text: 'with the episode'

    fill_in 'Original episode', with: 'original'
    find('#original input').click
    find('#original .dropdown-menu li', match: :first).click

    assert_difference '@show.episodes.count', -1 do
      click_on 'Merge'
      assert_text 'Duplicate Episode has been merged with Original Episode'
    end

    original.reload
    assert_equal 'A nice description.', original.description
  end
end
