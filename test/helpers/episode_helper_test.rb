# frozen_string_literal: true

require 'test_helper'

class EpisodeHelperTest < ActionView::TestCase
  test 'should get show image' do
    test_image1 = File.open('test/fixtures/files/images/timcast.png')
    test_image2 = File.open('test/fixtures/files/images/ben-shapiro.jpg')
    URI::HTTP.any_instance.stubs(:open).returns(test_image1).then.returns(test_image2)
    show = create(:show, image: nil)
    episode = create(:episode, show: show, image: nil)

    # fallback to placeholder
    assert_equal '/images/placeholder-show.svg', episode_image(episode)

    # fallback to show
    show.attach_image 'http://example.com/show-image.jpg'
    assert_match 'show-image.jpg', File.basename(episode_image(episode))

    episode.attach_image 'http://example.com/episode-image.jpg'
    assert_match 'episode-image.jpg', File.basename(episode_image(episode))
  end
end
