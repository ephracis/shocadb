# frozen_string_literal: true

require 'test_helper'

class ShowHelperTest < ActionView::TestCase
  test 'should get show image' do
    test_image = File.open('test/fixtures/files/images/timcast.png')
    URI::HTTP.any_instance.expects(:open).returns(test_image)
    show = create(:show, image: nil)
    assert_equal '/images/placeholder-show.svg', show_image(show)
    show.attach_image 'http://example.com/image.jpg'
    assert_match 'image.jpg', File.basename(show_image(show))
  end
end
