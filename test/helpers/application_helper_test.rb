# frozen_string_literal: true

require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  attr_reader :request

  test 'navbar_link generates normal link' do
    request.path = '/'
    actual = navbar_link('Test', '#', '/test')
    expected = link_to 'Test', '#', class: 'nav-link d-flex flex-row'
    assert_equal expected, actual
  end

  test 'navbar_link generates active link based on path' do
    request.path = '/shows'
    actual = navbar_link('Test', '#', '/shows')
    expected = link_to 'Test', '#', class: 'nav-link d-flex flex-row active'
    assert_equal expected, actual
  end

  test 'navbar_link generates active link based on controller' do
    request.path = '/shows'
    controller.params[:controller] = :shows
    actual = navbar_link('Test', '#', controller: :shows)
    expected = link_to 'Test', '#', class: 'nav-link d-flex flex-row active'
    assert_equal expected, actual
  end

  test 'navbar_link generates active link based on action' do
    request.path = '/shows'
    controller.params[:action] = :index
    actual = navbar_link('Test', '#', action: :index)
    expected = link_to 'Test', '#', class: 'nav-link d-flex flex-row active'
    assert_equal expected, actual
  end

  test 'navbar_link generates active link based on controller and action' do
    request.path = '/shows'
    actual = navbar_link('Test', '#', controller: :shows, action: :index)
    expected = link_to 'Test', '#', class: 'nav-link d-flex flex-row active'
    assert_equal expected, actual
  end

  test 'generate link to share on twitter' do
    expected = 'https://twitter.com/share?text=This+is+google+a+search+engine&url=https%3A%2F%2Fwww.google.com'
    url = 'https://www.google.com'
    text = 'This is google a search engine'
    assert_equal expected, link_to_share(:twitter, url, text)
  end

  test 'generate link to share on facebook' do
    expected = 'https://facebook.com/sharer.php?u=https%3A%2F%2Fwww.google.com&p[title]=This+Is%2C+Google+a+search+engine'
    url = 'https://www.google.com'
    text = 'This Is, Google a search engine'
    assert_equal expected, link_to_share(:facebook, url, text)
  end

  test 'generate link to pin on pinterest' do
    expected = 'https://pinterest.com/pin/create/button/?url=https%3A%2F%2Fwww.google.com&description=This+is+google+a+search+engine'
    url = 'https://www.google.com'
    text = 'This is google a search engine'
    assert_equal expected, link_to_share(:pinterest, url, text)
  end

  test 'generate link to share on unsupported service' do
    assert_raises ArgumentError do
      link_to_share(:invalid, 'url', 'text')
    end
  end
end
