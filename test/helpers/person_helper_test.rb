# frozen_string_literal: true

require 'test_helper'

class PersonHelperTest < ActionView::TestCase
  test 'should get person image' do
    test_image = File.open('test/fixtures/files/images/timcast.png')
    URI::HTTP.any_instance.expects(:open).returns(test_image)
    person = create(:person, image: nil)
    assert_equal '/images/placeholder-person.jpg', person_image(person)
    person.attach_image 'http://example.com/image.jpg'
    assert_match 'image.jpg', File.basename(person_image(person))
  end
end
