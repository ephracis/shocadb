# frozen_string_literal: true

require 'test_helper'

class EpisodeTest < ActiveSupport::TestCase
  include ConfirmableTests
  include BlockableTests
  include TopicableTests
  include ImageableTests
  include RateableTests
  include AliasableTests

  setup do
    @factory_name = :episode
    @aliased_field = :title
    URI::HTTP.any_instance.stubs(:open).returns(File.open('test/fixtures/files/images/timcast.png'))
  end

  test 'should save episode' do
    episode = build(:episode)
    assert episode.valid?
    assert episode.save
  end

  test 'should cast to string' do
    episode = create(:episode, title: 'My Episode')
    assert_equal 'My Episode', episode.to_s
  end

  test 'should not save episode with same slug' do
    show = create(:show)
    create(:episode, show: show, slug: 'test_1', title: 'Test 1')
    episode2 = build(:episode, show: show, slug: 'test_1', title: 'Test 2')

    assert_not episode2.valid?

    assert_no_difference('Episode.count', 'Created episode with duplicate slug in same show') do
      assert_raises(ActiveRecord::RecordInvalid) do
        episode2.save!
      end
    end

    episode2.slug = 'test_2'

    assert episode2.valid?, 'Episode is not valid after slug change'
    assert episode2.save!, 'Could not save episode after slug change'
  end

  test 'should save episode with same slug in different show' do
    show1 = create(:show)
    show2 = create(:show)

    create(:episode, show: show1, slug: 'test_1', title: 'Test 1')
    episode2 = build(:episode, show: show2, slug: 'test_1', title: 'Test 1')

    assert episode2.valid?, 'Episode is not valid with same slug, but different show'
    assert episode2.save, 'Could not save episode with same slug, but different show'
  end

  test 'should generate slug' do
    create(:episode, title: 'Test Episode', slug: 'test-episode')
    create(:episode, title: 'Test Episode 1', slug: 'test-episode1')

    episode = build(:episode, title: 'Test-Episode', slug: nil)
    assert_not episode.valid?
    episode.generate_slug
    assert episode.valid?
    assert_equal 'test-episode2', episode.slug
  end

  test 'should generate full title' do
    episode = create(:episode, title: 'My Episode', show: create(:show, title: 'My Show'))
    assert_equal 'My Show - My Episode', episode.full_title
  end

  test 'should create from source json' do
    create(:user)
    show = create(:show)
    create(:episode, show: show, title: 'Episode 1')
    source_json = {
      'source' => { 'kind' => 'spotify' },
      'episodes' => [
        {
          'description' => 'some description',
          'source_id' => 'spotify-id-1',
          'image' => 'http://example.com/spotify-image.jpg',
          'title' => 'episode 1',
          'aired_at' => '2021-09-01',
          'guests' => []
        },
        {
          'description' => "<p>link:\n\n<a href=\"#\">click</a>.</p>",
          'source_id' => 'spotify-id-2',
          'image' => 'http://example.com/spotify-image.jpg',
          'title' => 'Episode 2',
          'aired_at' => '2021-09-02',
          'guests' => ['Guest 1', 'Guest 2']
        }
      ]
    }

    assert_difference 'show.episodes.count', 1 do
      show.episodes.from_source source_json
    end

    episode = show.episodes.last
    assert_equal 'Episode 2', episode.title
    assert_equal 2, episode.people.count
    assert_equal 'Guest 1', episode.people.first.name
    assert_equal 'Guest 2', episode.people.last.name
    assert_equal "<p>link:\n\n<a href=\"#\">click</a>.</p>", episode.description
  end

  test 'should use episode aliases when generating from json' do
    create(:user)
    show = create(:show)
    episode = create(:episode, show: show, title: 'Episode 1')
    episode_on_other_show = create(:episode, title: 'Episode 1')
    create(:alias, aliasable: episode, name: 'Episode One')
    create(:alias, aliasable: episode_on_other_show, name: 'Episode One')
    source_json = {
      'source' => { 'kind' => 'spotify' },
      'episodes' => [
        {
          'description' => 'some description',
          'source_id' => 'spotify-id-1',
          'image' => 'http://example.com/spotify-image.jpg',
          'title' => 'episode one',
          'aired_at' => '2021-09-01',
          'guests' => []
        },
        {
          'description' => "<p>link:\n\n<a href=\"#\">click</a>.</p>",
          'source_id' => 'spotify-id-2',
          'image' => 'http://example.com/spotify-image.jpg',
          'title' => 'Episode 2',
          'aired_at' => '2021-09-02',
          'guests' => ['Guest 1', 'Guest 2']
        }
      ]
    }

    assert_difference 'show.episodes.count', 1 do
      show.episodes.from_source source_json
    end

    episode.reload
    assert_equal 2, show.episodes.count
    assert_equal 'Episode 1', episode.title
  end

  test 'should use person aliases when generating from json' do
    create(:user)
    show = create(:show)
    person = create(:person, name: 'Test Person')
    create(:alias, aliasable: person, name: 'Test Alias')
    source_json = {
      'source' => { 'kind' => 'spotify' },
      'episodes' => [
        {
          'description' => 'some description',
          'source_id' => 'spotify-id',
          'image' => 'http://example.com/spotify-image.jpg',
          'title' => 'Test Episode',
          'aired_at' => '2021-09-02',
          'guests' => ['Test alias']
        }
      ]
    }

    assert_no_difference 'Person.count' do
      show.episodes.from_source source_json
    end

    episode = show.episodes.last
    assert_equal 1, episode.people.count
    assert_equal person, episode.people.first
  end

  test 'should should skip guests for existing episodes' do
    create(:user)
    show = create(:show)
    person = create(:person, name: 'Test Person')
    episode = create(:episode, show: show, title: 'Test Episode')
    create(:appearance, episode: episode, person: person)

    source_json = {
      'source' => { 'kind' => 'spotify' },
      'episodes' => [
        {
          'description' => 'some description',
          'source_id' => 'spotify-id',
          'image' => 'http://example.com/spotify-image.jpg',
          'title' => 'Test   Episode',
          'aired_at' => '2021-09-02',
          'guests' => ['New Guest']
        }
      ]
    }

    assert_no_difference 'Person.count' do
      show.episodes.from_source source_json
    end

    episode = show.episodes.last
    assert_equal 1, episode.people.count
    assert_equal person, episode.people.first
  end

  test 'should merge sources when generating from json' do
    create(:user)
    show = create(:show)
    spotify_json = {
      'source' => { 'kind' => 'spotify' },
      'episodes' => [
        {
          'description' => 'some description',
          'source_id' => 'spotify-id',
          'image' => 'http://example.com/spotify-image.jpg',
          'title' => 'Test  Episode',
          'aired_at' => '2021-09-02',
          'guests' => ['Test Guest']
        }
      ]
    }
    youtube_json = {
      'source' => { 'kind' => 'youtube' },
      'episodes' => [
        {
          'description' => 'some description',
          'source_id' => 'youtube-id',
          'image' => 'http://example.com/youtube-image.jpg',
          'title' => 'Test Episode  ',
          'aired_at' => '2021-09-02',
          'guests' => ['Test Guest']
        }
      ]
    }

    show.episodes.from_source spotify_json

    assert_no_difference 'Episode.count' do
      show.episodes.from_source youtube_json
    end

    episode = show.episodes.last
    assert_equal 1, episode.people.count
    assert_equal 'youtube-id', episode.youtube
    assert_equal 'spotify-id', episode.spotify
  end

  test 'should mark episode as duplicate' do
    episode1 = create(:episode, title: 'Episode One', description: nil)
    episode2 = create(:episode, title: 'Episode Two', description: 'A nice description.')

    assert_difference 'Episode.count', -1 do
      episode2.duplicate_of! episode1
    end

    assert_equal 'Episode One', episode1.title
    assert_equal 'A nice description.', episode1.description
    assert_equal episode1, Slug.find_by(name: episode2.slug, slugable_type: 'Episode').slugable
    assert_equal episode1, Episode.find_by_alias(episode2.title)
  end

  test 'should not mark as duplicate of a person' do
    person = create(:person)
    episode = create(:episode)

    assert_raises RuntimeError do
      episode.duplicate_of! person
    end
  end
end
