# frozen_string_literal: true

require 'test_helper'

class AppearanceTest < ActiveSupport::TestCase
  test 'person can appear on episode' do
    person = create(:person)
    episode = create(:episode)
    appearance = Appearance.new(person: person, episode: episode)

    assert appearance.valid?
    assert appearance.save
  end

  test 'person cannot appear on episode twice' do
    person = create(:person)
    episode = create(:episode)
    Appearance.create(person: person, episode: episode)
    appearance = Appearance.new(person: person, episode: episode)

    assert_not appearance.valid?
    assert_not appearance.save
  end

  test 'can access people appeared on episode' do
    people = create_list(:person, 5)
    episodes = create_list(:episode, 3)

    create(:appearance, episode: episodes[0], person: people[0])
    create(:appearance, episode: episodes[0], person: people[1])
    create(:appearance, episode: episodes[0], person: people[2])
    create(:appearance, episode: episodes[1], person: people[2])
    create(:appearance, episode: episodes[1], person: people[3])

    assert_equal 3, episodes[0].people.count
    assert_equal 2, episodes[1].people.count
    assert_equal 0, episodes[2].people.count

    assert_equal people[0], episodes[0].people.first
    assert_equal people[2], episodes[1].people.first
  end

  test 'can access episodes and shows for a person' do
    shows = create_list(:show, 2)
    people = create_list(:person, 2)
    create_list(:episode, 2, show: shows[0])
    create_list(:episode, 2, show: shows[1])

    create(:appearance, person: people[0], episode: shows[0].episodes[0])
    create(:appearance, person: people[0], episode: shows[0].episodes[1])
    create(:appearance, person: people[0], episode: shows[1].episodes[0])

    assert_equal 3, people[0].episodes.count
    assert_equal 2, people[0].shows.count
    assert_equal 0, people[1].episodes.count
    assert_equal 0, people[1].shows.count

    assert_equal shows[0], people[0].shows.first
    assert_equal shows[0].episodes.order(:id).first, people[0].episodes.order(:id).first
  end
end
