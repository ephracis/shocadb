# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def omniauth_for(provider)
    json = File.read("test/fixtures/files/omniauth/#{provider}.json")
    JSON.parse json, object_class: OpenStruct
  end

  test 'should create user' do
    assert_difference('User.count', 1, "Didn't create user") do
      User.create(
        email: 'foo@bar.com',
        time_zone: 'UTC',
        username: 'myuser',
        password: 'changeme'
      )
    end

    assert_no_difference('User.count', 'Created user with duplicate mail') do
      assert_raises(ActiveRecord::RecordInvalid) do
        User.create!(
          email: 'foo@bar.com',
          time_zone: 'UTC',
          username: 'auser',
          password: 'changeme'
        )
      end
    end
  end

  test 'should create from google auth' do
    assert_difference('User.count', 1, "Didn't create user") do
      auth = omniauth_for(:google)
      user = User.from_omniauth auth
      assert_equal 'john@example.com', user.email
      assert_equal 'john@example.com', auth.info.email
      assert_equal 'John Smith', user.fullname
      assert_equal 'John Smith', auth.info.name
      assert_equal 'john', user.username
      assert user.persisted?
    end
  end

  test 'should cast to string' do
    user = create(:user, username: 'mr.user', fullname: '')
    assert_equal 'mr.user', user.to_s
    user.update(fullname: 'Mr User')
    assert_equal 'Mr User', user.to_s
  end

  test 'should validate time zone' do
    user = User.new(
      email: 'foo@bar.com',
      fullname: 'My User',
      username: 'myuser',
      password: 'changeme'
    )

    user.time_zone = 'invalid'
    assert_not user.valid?

    user.time_zone = 'Stockholm'
    assert user.valid?
  end

  test 'should get voted shows' do
    user = create(:user)
    show1 = create(:show)
    show2 = create(:show)
    show3 = create(:show)

    create(:vote, user: user, show: show1, value: 1)
    create(:vote, user: user, show: show1, value: 1)
    create(:vote, user: user, show: show2, value: -1)

    assert_equal 2, user.shows_voted_on.length
    assert_includes user.shows_voted_on, show1
    assert_includes user.shows_voted_on, show2
    assert_not_includes user.shows_voted_on, show3
  end

  test 'should get voted guests' do
    user = create(:user)
    guest1 = create(:person)
    guest2 = create(:person)
    guest3 = create(:person)

    create(:vote, user: user, person: guest1, value: 1)
    create(:vote, user: user, person: guest1, value: 1)
    create(:vote, user: user, person: guest2, value: -1)

    assert_equal 2, user.people_voted_on.length
    assert_includes user.people_voted_on, guest1
    assert_includes user.people_voted_on, guest2
    assert_not_includes user.people_voted_on, guest3
  end

  test 'should find for database authentication' do
    user = create(:user, email: 'test@mail.com', username: 'test')
    assert_equal user, User.find_for_database_authentication({ email: +'test@MAIL.com' })
    assert_equal user, User.find_for_database_authentication({ login: +'test@MAIL.com' })
    assert_equal user, User.find_for_database_authentication({ username: 'test' })
    assert_equal user, User.find_for_database_authentication({ login: 'TEST' })
  end
end
