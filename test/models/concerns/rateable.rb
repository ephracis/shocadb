# frozen_string_literal: true

module RateableTests
  extend ActiveSupport::Testing::Declarative

  test 'should get ordered by rating' do
    resources = create_list(@factory_name, 6)

    # resource[0] average 3
    create(:rating, rateable: resources[0], value: 4)
    create(:rating, rateable: resources[0], value: 2)

    # resource[1] average 4.5
    create(:rating, rateable: resources[1], value: 4)
    create(:rating, rateable: resources[1], value: 5)

    # resource[2] average 2
    create(:rating, rateable: resources[2], value: 1)
    create(:rating, rateable: resources[2], value: 2)
    create(:rating, rateable: resources[2], value: 3)

    # resource[3] average 1
    create(:rating, rateable: resources[3], value: 1)

    # resource[4] average 5
    create(:rating, rateable: resources[4], value: 5)
    create(:rating, rateable: resources[4], value: 5)

    # resource[5] no ratings

    expected_order = [resources[4], resources[1], resources[0], resources[2], resources[3]]

    assert_equal expected_order.map(&:id), resources[0].class.order_by_ratings.map(&:id)
  end

  test 'should include average rating in query' do
    resources = create_list(@factory_name, 6)

    # resource[0] average 3
    create(:rating, rateable: resources[0], value: 4)
    create(:rating, rateable: resources[0], value: 2)

    # resource[1] average 4.5
    create(:rating, rateable: resources[1], value: 4)
    create(:rating, rateable: resources[1], value: 5)

    # resource[2] average 2
    create(:rating, rateable: resources[2], value: 1)
    create(:rating, rateable: resources[2], value: 2)
    create(:rating, rateable: resources[2], value: 3)

    # resource[3] average 1
    create(:rating, rateable: resources[3], value: 1)

    # resource[4] average 5
    create(:rating, rateable: resources[4], value: 5)
    create(:rating, rateable: resources[4], value: 5)

    # resource[5] no ratings

    resources = resources[0].class.with_ratings.order(:id)
    assert_equal 3, resources[0].average_rating
    assert_equal 4.5, resources[1].average_rating
    assert_equal 2, resources[2].average_rating
    assert_equal 1, resources[3].average_rating
    assert_equal 5, resources[4].average_rating
    assert_equal nil, resources[5].average_rating
  end
end
