# frozen_string_literal: true

module AliasableTests
  extend ActiveSupport::Testing::Declarative

  test 'should find by aliases' do
    resource = create(@factory_name, { @aliased_field => 'Test Name' })
    resource.aliases.create(name: 'Test Alias 1')
    resource.aliases.create(name: 'Test Alias 2')

    assert_equal resource, resource.class.find_by_alias('Test Name')
    assert_equal resource, resource.class.find_by_alias('Test Alias 1')
    assert_equal resource, resource.class.find_by_alias('Test Alias 2')
    assert_equal nil, resource.class.find_by_alias('Test Alias 3')
  end
end
