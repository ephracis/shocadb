# frozen_string_literal: true

require 'test_helper'

class TopicTest < ActiveSupport::TestCase
  test 'should not save duplicates' do
    create(:topic, name: 'Test')
    topic = build(:topic, name: 'Test')
    assert_not topic.valid?
    assert_not topic.save
  end
end
