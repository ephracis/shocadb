# frozen_string_literal: true

require 'test_helper'

class NotificationMailerTest < ActionMailer::TestCase
  test 'appearance' do
    user = create(:user, email: 'test@mail.com')
    notification = create(:notification, user: user, title: 'Test Notification', content: 'A <a href="#">test</a>.')
    email = NotificationMailer.with(notification: notification).appearance

    assert_emails 1 do
      email.deliver_now
    end

    assert_equal ['notifications@shocadb.com'], email.from
    assert_equal ['test@mail.com'], email.to
    assert_equal 'Test Notification', email.subject
    assert_equal read_fixture('appearance.txt').join.strip, email.text_part.body.to_s.strip
    assert_equal read_fixture('appearance.html').join, email.html_part.body.to_s
  end
end
