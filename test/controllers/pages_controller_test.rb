# frozen_string_literal: true

require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test 'should get welcome' do
    get welcome_url
    assert_response :success
  end

  test 'should get privacy' do
    get privacy_url
    assert_response :success
  end

  test 'should get terms' do
    get terms_url
    assert_response :success
  end

  test 'should get sitemap' do
    get sitemap_url
    assert_response :redirect
  end
end
