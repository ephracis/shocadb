# frozen_string_literal: true

require 'test_helper'

class Admin::UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = create(:root_user)
    sign_in @user
  end

  test 'should get index' do
    get admin_users_url
    assert_response :success
  end

  test 'should get show' do
    user = create(:user)
    get admin_user_url(user)
    assert_response :success
  end

  test 'should update user' do
    user = create(:user)
    patch admin_user_url(user), params: { user: { username: 'new_name' } }
    assert_redirected_to admin_user_url(user)
    assert_equal 'new_name', User.find(user.id).username
  end

  test 'should update user confirmation' do
    user = create(:user, confirmed_at: nil)
    assert_not user.confirmed?
    patch admin_user_url(user, format: :json), params: { user: { confirmed: 1 } }
    assert User.find(user.id).confirmed?
  end

  test 'should fail with invalid username' do
    user = create(:user)
    patch admin_user_url(user), params: { user: { username: 'x' } }
    assert_not_equal 'x', User.find(user.id).username
  end
end
