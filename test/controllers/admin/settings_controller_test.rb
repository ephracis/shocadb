# frozen_string_literal: true

require 'test_helper'

class Admin::SettingsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = create(:root_user)
    sign_in @user
  end

  test 'should get edit' do
    get admin_settings_url
    assert_response :success
  end

  test 'should get edit without any resources' do
    ApplicationSettings.delete_all
    assert_difference 'ApplicationSettings.count' do
      get admin_settings_url
      assert_response :success
    end
  end

  test 'should update settings' do
    create(:application_settings, max_votes_per_show: 5)
    put admin_settings_url, params: { application_settings: { max_votes_per_show: 1 } }
    assert_redirected_to admin_settings_url
    assert_equal 1, ApplicationSettings.last.max_votes_per_show
  end

  test 'should not update invalid settings' do
    create(:application_settings, max_votes_per_show: 5)
    put admin_settings_url, params: { application_settings: { max_votes_per_show: -2 } }
    assert_response :success
    assert_equal 5, ApplicationSettings.last.max_votes_per_show
  end
end
