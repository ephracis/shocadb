# frozen_string_literal: true

module BlockableControllerTests
  extend ActiveSupport::Testing::Declarative

  test 'should allow root to block resource' do
    user = create(:user, roles: [Role.find_or_create_by(name: :root, title: 'Root')])
    resource = create(@factory_name, blocked_at: nil, blocked_by: nil)

    assert_not resource.blocked?

    sign_in user
    params = {}
    params[resource.class.to_s.underscore] = { blocked: 1 }
    patch url_for(resource), params: params
    assert_response :redirect
    assert resource.class.find(resource.id).blocked?
  end

  test 'should allow root to unblock resource' do
    user = create(:user, roles: [Role.find_or_create_by(name: :root, title: 'Root')])
    resource = create(@factory_name, blocked_at: DateTime.now, blocked_by: user)

    assert resource.blocked?

    sign_in user
    params = {}
    params[resource.class.to_s.underscore] = { blocked: 0 }
    patch url_for(resource), params: params
    assert_response :redirect
    assert_not resource.class.find(resource.id).blocked?
  end

  test 'should not allow normal user to block' do
    user = create(:user, roles: [])
    resource = create(@factory_name, blocked_at: nil, blocked_by: nil)

    assert_not resource.blocked?

    sign_in user
    params = {}
    params[resource.class.to_s.underscore] = { blocked: 1 }
    patch url_for(resource), params: params
    assert_redirected_to root_url
    assert_not resource.class.find(resource.id).blocked?
  end

  test 'should not allow normal user to unblock' do
    user = create(:user, roles: [])
    resource = create(@factory_name, blocked_at: DateTime.now, blocked_by: user)

    assert resource.blocked?

    sign_in user
    params = {}
    params[resource.class.to_s.underscore] = { blocked: 0 }
    patch url_for(resource), params: params
    assert_redirected_to root_url
    assert resource.class.find(resource.id).blocked?
  end
end
