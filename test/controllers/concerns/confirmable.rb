# frozen_string_literal: true

module ConfirmableControllerTests
  extend ActiveSupport::Testing::Declarative

  test 'should allow root to confirm resource' do
    user = create(:user, roles: [Role.find_or_create_by(name: :root, title: 'Root')])
    resource = create(@factory_name, confirmed_at: nil, confirmed_by: nil)

    assert_not resource.confirmed?

    sign_in user
    params = {}
    params[resource.class.to_s.underscore] = { confirmed: 1 }
    patch url_for(resource), params: params
    assert_redirected_to admin_unconfirmed_url
    assert resource.class.find(resource.id).confirmed?
  end

  test 'should allow root to unconfirm resource' do
    user = create(:user, roles: [Role.find_or_create_by(name: :root, title: 'Root')])
    resource = create(@factory_name, confirmed_at: DateTime.now, confirmed_by: user)

    assert resource.confirmed?

    sign_in user
    params = {}
    params[resource.class.to_s.underscore] = { confirmed: 0 }
    patch url_for(resource), params: params
    assert_redirected_to admin_unconfirmed_path
    assert_not resource.class.find(resource.id).confirmed?
  end

  test 'should not allow normal user to confirm' do
    user = create(:user, roles: [])
    resource = create(@factory_name, confirmed_at: nil, confirmed_by: nil)

    assert_not resource.confirmed?

    sign_in user
    params = {}
    params[resource.class.to_s.underscore] = { confirmed: 1 }
    patch url_for(resource), params: params
    assert_redirected_to root_url
    assert_not resource.class.find(resource.id).confirmed?
  end

  test 'should not allow normal user to unconfirm' do
    user = create(:user, roles: [])
    resource = create(@factory_name, confirmed_at: DateTime.now, confirmed_by: user)

    assert resource.confirmed?

    sign_in user
    params = {}
    params[resource.class.to_s.underscore] = { confirmed: 0 }
    patch url_for(resource), params: params
    assert_redirected_to root_url
    assert resource.class.find(resource.id).confirmed?
  end
end
