# frozen_string_literal: true

module TopicableControllerTests
  extend ActiveSupport::Testing::Declarative

  test 'should set topics' do
    resource = create(@factory_name)
    resource.topics.create(name: 'Topic 1', slug: 'topic-1')
    resource.topics.create(name: 'Topic 2', slug: 'topic-2')
    Topic.create(name: 'Topic 3', slug: 'topic-3')
    assert_equal 2, resource.topics.count
    params = {}
    params[resource.class.to_s.underscore] = { topics: ['Topic 2', 'Topic 3', 'Topic 4'] }
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user

    patch url_for(resource), params: params
    resource.reload

    assert_response :redirect
    assert_equal 3, resource.topics.count
    assert_equal 4, Topic.count
    assert_equal 'Topic 2', resource.topics[0].name
    assert_equal 'Topic 3', resource.topics[1].name
    assert_equal 'Topic 4', resource.topics[2].name
  end

  test 'should keep topics when parameter is missing' do
    resource = create(@factory_name)
    resource.topics.create(name: 'Topic 1', slug: 'topic-1')
    resource.topics.create(name: 'Topic 2', slug: 'topic-2')
    params = {}
    params[resource.class.to_s.underscore] = { description: 'new description' }
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user

    patch url_for(resource), params: params
    resource.reload

    assert_response :redirect
    assert_equal 2, resource.topics.count
    assert_equal 'Topic 1', resource.topics[0].name
    assert_equal 'Topic 2', resource.topics[1].name
  end

  test 'should create resource with topics' do
    resource = build(@factory_name)
    resource_param = resource.class.to_s.parameterize
    resource_url = "#{resource.class.to_s.tableize}_url"

    params = {}
    params[resource_param] = build(@factory_name).attributes.compact
    params[resource_param][:topics] = ['Topic 2', 'Topic 3', 'Topic 4']

    sign_in create(:root_user)

    assert_difference("#{resource.class}.count") do
      assert_difference('Topic.count', 3) do
        post send(resource_url), params: params
      end
    end

    resource = resource.class.last
    assert_equal 3, resource.topics.count
  end
end
