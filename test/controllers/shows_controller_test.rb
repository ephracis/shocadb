# frozen_string_literal: true

require 'test_helper'

class ShowsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ConfirmableControllerTests
  include BlockableControllerTests
  include TopicableControllerTests
  include RateableControllerTests
  include EditableControllerTests
  include ImageableControllerTests

  setup do
    @factory_name = :show
    @editable_params = %i[title description]
  end

  test 'should get index' do
    get shows_url
    assert_response :success
  end

  test 'should get new' do
    get new_show_url
    assert_redirected_to new_user_session_url

    sign_in create(:user)
    get new_show_url
    assert_response :success
  end

  test 'should create show' do
    params = { show: { title: 'Test Show' } }

    post shows_url, params: params
    assert_redirected_to new_user_session_url

    sign_in create(:user)
    assert_difference('Show.count') do
      post shows_url, params: params
      assert_redirected_to show_url(Show.last)
      assert_equal 'test-show', Show.last.slug
    end
  end

  test 'should not create invalid show' do
    sign_in create(:user)
    assert_no_difference('Show.count') do
      post shows_url, params: { show: { slug: 'wrong slug' } }
    end
  end

  test 'should show show' do
    show = create(:show)
    get show_url(show)
    assert_response :success
  end

  test 'should get edit' do
    show = create(:show)
    get edit_show_url(show)
    assert_redirected_to new_user_session_url

    sign_in show.created_by
    get edit_show_url(show)
    assert_response :success
  end

  test 'should update show' do
    params = { show: { slug: 'new_slug' } }
    show = create(:show)
    patch show_url(show), params: params
    assert_redirected_to new_user_session_url

    sign_in show.created_by
    patch show_url(show), params: params
    show = Show.find(show.id)
    assert_redirected_to show_url(show)
    assert_equal 'new_slug', show.slug
  end

  test 'should not update with invalid slug' do
    show = create(:show)
    sign_in show.created_by
    patch show_url(show), params: { show: { slug: 'wrong slug' } }
    show = Show.find(show.id)
    assert_not_equal 'wrong slug', show.slug
  end

  test 'should set hosts' do
    show = create(:show)
    person1 = create(:person)
    person2 = create(:person)
    params = { show: { hosts: [person1.id, person2.id] } }
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user

    patch show_url(show), params: params
    show = Show.find(show.id)
    person1 = Person.find(person1.id)
    person2 = Person.find(person2.id)

    assert_redirected_to show_url(show)
    assert_equal 2, show.hosts.count
    assert_equal 1, person1.hosted_shows.count
    assert_equal 1, person2.hosted_shows.count
    assert_equal 2, Hosting.count
    assert_includes show.hosts, person1
    assert_includes show.hosts, person2
    assert_includes person1.hosted_shows, show
    assert_includes person2.hosted_shows, show
  end

  test 'should keep hosts when parameter is missing' do
    show = create(:show)
    person1 = create(:person)
    person2 = create(:person)
    create(:hosting, show: show, person: person1)
    create(:hosting, show: show, person: person2)
    params = { show: { description: 'new description' } }
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user

    patch show_url(show), params: params
    show = Show.find(show.id)
    person1 = Person.find(person1.id)
    person2 = Person.find(person2.id)

    assert_redirected_to show_url(show)
    assert_equal 2, show.hosts.count
    assert_equal 1, person1.hosted_shows.count
    assert_equal 1, person2.hosted_shows.count
    assert_equal 2, Hosting.count
    assert_includes show.hosts, person1
    assert_includes show.hosts, person2
    assert_includes person1.hosted_shows, show
    assert_includes person2.hosted_shows, show
  end

  test 'should create person when setting hosts' do
    show = create(:show)
    person1 = create(:person)
    create(:alias, name: 'Alias', aliasable: person1)
    params = { show: { hosts: ['Alias', 'New Person'] } }
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user

    assert_difference 'Person.count', 1 do
      patch show_url(show), params: params
    end

    show = Show.find(show.id)
    person1 = Person.find(person1.id)
    person2 = Person.last

    assert_redirected_to show_url(show)
    assert_equal 2, show.hosts.count
    assert_equal 1, person1.hosted_shows.count
    assert_equal 1, person2.hosted_shows.count
    assert_equal 2, Hosting.count
    assert_includes show.hosts, person1
    assert_includes show.hosts, person2
    assert_includes person1.hosted_shows, show
    assert_includes person2.hosted_shows, show
  end

  test 'should destroy show' do
    show = create(:show)
    delete show_url(show)
    assert_redirected_to new_user_session_url

    sign_in create(:root_user)
    assert_difference('Show.count', -1) do
      delete show_url(show)
      assert_redirected_to shows_url
    end
  end
end
