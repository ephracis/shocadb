# frozen_string_literal: true

require 'test_helper'

class NotificationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = create(:user)
    sign_in @user
  end

  test 'should get index' do
    create_list(:read_notification, 2, user: @user)
    create_list(:unread_notification, 3, user: @user)
    get notifications_url(format: :json)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 5, json.length
  end

  test 'should mark all as read' do
    create_list(:read_notification, 2, user: @user)
    create_list(:unread_notification, 3, user: @user)
    patch read_notifications_url(format: :json)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 5, json.length
    assert_equal 5, @user.notifications.where(read: true).count
    assert_equal 0, @user.notifications.where(read: false).count
  end

  test 'should not destroy someone elses notification' do
    notification = create(:notification)
    assert_no_difference('Notification.count') do
      delete notification_url(notification)
      assert_redirected_to root_url
    end
  end

  test 'should destroy notification' do
    notification = create(:notification, user: @user)
    assert_difference('@user.notifications.count', -1) do
      delete notification_url(notification)
      assert_redirected_to notifications_url
    end
  end
end
