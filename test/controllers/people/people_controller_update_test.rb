# frozen_string_literal: true

require 'test_helper'

class PeopleControllerIndexTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should get edit' do
    person = create(:person)

    get edit_person_url(person)
    assert_redirected_to new_user_session_url

    sign_in person.created_by
    get edit_person_url(person)
    assert_response :success
  end

  test 'should update person' do
    person = create(:person)
    params = { person: { description: person.description } }

    patch person_url(person), params: params
    assert_redirected_to new_user_session_url

    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    patch person_url(person), params: params
    assert_redirected_to person_url(person)
  end

  test 'should not update person with invalid name' do
    person = create(:person)
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    patch person_url(person), params: { person: { name: 'x' } }
    person = Person.find(person.id)
    assert_not_equal 'x', person.name
  end

  test 'should set aliases' do
    person = create(:person)
    person.aliases.create(name: 'Alias 1')
    person.aliases.create(name: 'Alias 2')
    params = { person: { aliases: ['Alias 2', 'Alias 3', 'Alias 4'] } }
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user

    patch person_url(person), params: params
    person = Person.find(person.id)

    assert_redirected_to person_url(person)
    assert_equal 3, person.aliases.count
    assert_equal 3, Alias.count
    assert_equal 'Alias 2', person.aliases[0].name
    assert_equal 'Alias 3', person.aliases[1].name
    assert_equal 'Alias 4', person.aliases[2].name
  end

  test 'should keep aliases when parameter is missing' do
    person = create(:person)
    person.aliases.create(name: 'Alias 1')
    person.aliases.create(name: 'Alias 2')
    params = { person: { description: 'my new description' } }
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user

    patch person_url(person), params: params
    person = Person.find(person.id)

    assert_redirected_to person_url(person)
    assert_equal 2, person.aliases.count
    assert_equal 'Alias 1', person.aliases[0].name
    assert_equal 'Alias 2', person.aliases[1].name
  end
end
