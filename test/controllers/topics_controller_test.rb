# frozen_string_literal: true

require 'test_helper'

class TopicsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should show topic' do
    topic = create(:topic)
    show = create(:show, topics: [topic])
    create(:episode, show: show, topics: [topic])
    person = create(:person, topics: [topic])
    create(:book, authors: [person], topics: [topic])

    get topic_url(topic)
    assert_response :success
  end
end
