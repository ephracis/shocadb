import { shallowMount } from '@vue/test-utils'
import TagInput from 'TagInput.vue'

function mountComponent (props = {}, options = null) {
  if (!options) {
    options = { propsData: props, stubs: ['font-awesome-icon'] }
  }

  return shallowMount(TagInput, options)
}

describe('TagInput', () => {
  it ('has a created hook', () => {
    expect(typeof TagInput.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof TagInput.data).toBe('function')
    const defaultData = TagInput.data()
    expect(defaultData.tags).toEqual([])
  })

  it('mounts properly', () => {
    const wrapper = mountComponent({ value: ['Foo'] })
    expect(wrapper.vm.$data.tags).toEqual(['Foo'])
  })

  it('removes tag', () => {
    const wrapper = mountComponent({ value: ['Foo'] })
    wrapper.vm.removeTag(0)
    expect(wrapper.vm.$data.tags).toEqual([])
  })

  it('adds tag', () => {
    const wrapper = mountComponent({ value: ['Foo'] })
    wrapper.vm.addTag(' Bar  ')
    expect(wrapper.vm.$data.tags).toEqual(['Foo', 'Bar'])
  })

  it('adds tag on enter', () => {
    const wrapper = mountComponent({ value: ['Foo'] })
    wrapper.vm.$data.newTag = '  Bar '
    wrapper.vm.onEnter()
    expect(wrapper.vm.$data.tags).toEqual(['Foo', 'Bar'])
    expect(wrapper.vm.$data.newTag).toEqual('')
  })

  describe('.onChange', () => {
    it('adds tag when comma', () => {
      const wrapper = mountComponent({ value: [] })
      wrapper.vm.$data.newTag = '  Foo  ,  ,Bar '
      wrapper.vm.onChange()
      expect(wrapper.vm.$data.tags).toEqual(['Foo', 'Bar'])
      expect(wrapper.vm.$data.newTag).toEqual('')
    })
    it('does nothing when no comma', () => {
      const wrapper = mountComponent({ value: [] })
      wrapper.vm.$data.newTag = '  Foo  .  !Bar '
      wrapper.vm.onChange()
      expect(wrapper.vm.$data.tags).toEqual([])
      expect(wrapper.vm.$data.newTag).toEqual('  Foo  .  !Bar ')
    })
  })
})
