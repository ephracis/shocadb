global.window = window
global.$ = function () {
  return { tab () {} }
}

jest.useFakeTimers('legacy')

import { shallowMount } from '@vue/test-utils'
import NotificationBell from 'NotificationBell.vue'

import axios from 'axios'
jest.mock('axios')

function mountComponent(response, options) {

  // mock axios request
  const defaultResponse = [
    { title: 'Notification 1', id: 1, content: 'Some content', image: 'img.jpg', read: false, created_ago: '1 second' },
    { title: 'Notification 2', id: 2, content: 'Some content', image: 'img.jpg', read: true, created_ago: '1 second' }
  ]
  const resp = { data: response || defaultResponse }
  axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  if (!options) {
    options = {
      propsData: {
        url: 'http://example.com'
      },
      stubs: ['font-awesome-layers', 'font-awesome-icon', 'font-awesome-layers-text']
    }
  }

  return shallowMount(NotificationBell, options)
}

describe('NotificationBell', () => {
  it ('has a mounted hook', () => {
    expect(typeof NotificationBell.mounted).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof NotificationBell.data).toBe('function')
    const defaultData = NotificationBell.data()
    expect(defaultData.notificationCount).toEqual(0)
  })

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.$data.notificationCount).toEqual(0)
  })

  describe('getting notification list', () => {
    it('sets notifications from URL', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.notificationCount).toEqual(1)
    })
  })
})
