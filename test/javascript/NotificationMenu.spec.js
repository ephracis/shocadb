global.window = window
global.$ = function () {
  return { tab () {} }
}

jest.useFakeTimers('legacy')

import { shallowMount } from '@vue/test-utils'
import NotificationMenu from 'NotificationMenu.vue'

import axios from 'axios'
jest.mock('axios')

import EventBus from 'eventBus'
jest.mock('eventBus')

function mountComponent(response, options) {

  // mock axios request
  const defaultResponse = [
    { title: 'Notification 1', id: 1, content: 'Some content', image: 'img.jpg', read: false, created_ago: '1 second' },
    { title: 'Notification 2', id: 2, content: 'Some content', image: 'img.jpg', read: true, created_ago: '1 second' }
  ]
  const resp = { data: response || defaultResponse }
  axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  if (!options) {
    options = {
      propsData: {
        url: 'http://example.com'
      },
      stubs: ['notification-list', 'font-awesome-layers', 'font-awesome-icon', 'font-awesome-layers-text']
    }
  }

  return shallowMount(NotificationMenu, options)
}

describe('NotificationMenu', () => {
  it ('has a mounted hook', () => {
    expect(typeof NotificationMenu.mounted).toBe('function')
  })

  it ('has a beforeDestroy hook', () => {
    expect(typeof NotificationMenu.beforeDestroy).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof NotificationMenu.data).toBe('function')
    const defaultData = NotificationMenu.data()
    expect(defaultData.notifications).toEqual([])
    expect(defaultData.notificationCount).toEqual(0)
  })

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.$data.notifications.length).toEqual(0)
  })

  it('observes class changes', async () => {

    const mockObserver = {
      attributeName: 'testAttributeName',
      oldValue: 'testOldValue',
      target: {
        getAttribute: jest.fn(() => 'testClass1 testClass2')
      }
    }

    const mockObserveFunc = jest.fn()
    const mockdisconnectFunc = jest.fn()

    global.MutationObserver = jest.fn().mockImplementation((foo) => {
      foo([mockObserver])
      return { observe: mockObserveFunc, disconnect: mockdisconnectFunc }
    })

    const wrapper = mountComponent()
    await wrapper.vm.$nextTick()
    expect(mockObserver.target.getAttribute.mock.calls[0][0]).toEqual('testAttributeName')
    expect(mockObserveFunc.mock.calls[0][1]).toEqual({
      attributes: true,
      attributeOldValue: true,
      attributeFilter: ['class']
    })

    expect(mockdisconnectFunc.mock.calls.length).toBe(0)
    wrapper.destroy()
    expect(mockdisconnectFunc.mock.calls.length).toBe(1)
  })

  describe('getting notification list', () => {

    it('sets notifications from URL', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.notifications.length).toEqual(2)
      expect(wrapper.vm.$data.notifications[0].id).toEqual(1)
      expect(wrapper.vm.$data.notifications[0].title).toEqual('Notification 1')
    })

    it('handle API errors', async () => {
      axios.get.mockRejectedValue('my error')
      const url = 'https://example.com/notifications.json'
      EventBus.$emit = jest.fn()
      const wrapper = shallowMount(NotificationMenu, {
        propsData: { url: url },
        stubs: ['notification-list', 'font-awesome-layers', 'font-awesome-icon', 'font-awesome-layers-text']
      })
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.notifications.length).toEqual(0)
      expect(EventBus.$emit.mock.calls.length).toBe(1)
      expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
      expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'Could get notifications: my error.', type: 'danger' })
    })
  })

  describe('.onClassChange', () => {
    it('starts timer', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.markAsReadTimer).toBeNull()
      wrapper.vm.onClassChange('test show')
      expect(wrapper.vm.$data.markAsReadTimer).not.toBeNull()
    })
  })

  describe('.markAsRead', () => {
    it('updates notifications', async () => {
      axios.patch.mockResolvedValue({ data: [{ id: 1, title: 'Test1', content: 'Test', image: 'test', read: true }] })
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.notifications.length).toEqual(2)
      expect(wrapper.vm.$data.notificationCount).toEqual(1)
      wrapper.vm.markAsRead()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.notifications.length).toEqual(1)
      expect(wrapper.vm.$data.notificationCount).toEqual(0)
    })

    it('handle API errors', async () => {
      axios.patch.mockRejectedValue('my error')
      EventBus.$emit = jest.fn()
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      wrapper.vm.markAsRead()
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      expect(EventBus.$emit.mock.calls.length).toBe(1)
      expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
      expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'Could not mark as read: my error.', type: 'danger' })
    })
  })

  describe('.removeNotification', () => {
    it('removes notifications', async () => {
      axios.delete.mockResolvedValue({})
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.notifications.length).toEqual(2)
      expect(wrapper.vm.$data.notificationCount).toEqual(1)
      wrapper.vm.removeNotification(wrapper.vm.$data.notifications[1])
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.notifications.length).toEqual(1)
      expect(wrapper.vm.$data.notificationCount).toEqual(0)
    })

    it('handle API errors', async () => {
      axios.delete.mockRejectedValue('my error')
      EventBus.$emit = jest.fn()
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      wrapper.vm.removeNotification(wrapper.vm.$data.notifications[1])
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      expect(EventBus.$emit.mock.calls.length).toBe(1)
      expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
      expect(EventBus.$emit.mock.calls[0][1]).toEqual(
        { message: 'Could not delete notification: my error.', type: 'danger' }
      )
    })
  })
})
