import { shallowMount } from '@vue/test-utils'
import Rating from 'Rating.vue'
import Axios from 'axios'
import EventBus from 'eventBus'

jest.useFakeTimers('legacy')
jest.mock('axios')
jest.mock('eventBus')
global.gtag = jest.fn()

function mountComponent (response, props) {

  // mock axios request
  const defaultResponse = { rating: 3, my_rating: -1 }
  const resp = { data: response || defaultResponse }
  Axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [{ getAttribute: function () { return 'my-secret-token' } }]
  })

  const defaultProps = {
    rateUrl: '/show/1/rate.json',
    ratingUrl: '/show/1/rating.json'
  }

  let options = { propsData: defaultProps, stubs: ['font-awesome-icon'] }
  if (props) { options.propsData = props }

  return shallowMount(Rating, options)
}

describe('Rating', () => {
  it('has a mounted hook', () => {
    expect(typeof Rating.mounted).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof Rating.data).toBe('function')
    const defaultData = Rating.data()
    expect(defaultData.rating).toEqual(0)
    expect(defaultData.myRating).toEqual(-1)
  })

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.$data.rating).toEqual(0)
  })

  describe('.mounted', () => {
    it('fetches rating data', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.rating).toEqual(3)
      expect(wrapper.vm.$data.myRating).toEqual(-1)
    })

    it('handles API errors', async () => {
      Axios.get.mockRejectedValue('rating error')
      EventBus.$emit = jest.fn()

      const props = { rateUrl: '/show/1/rate.json', ratingUrl: '/show/1/rating.json' }
      const wrapper = shallowMount(Rating, { propsData: props, stubs: ['font-awesome-icon'] })
      await wrapper.vm.$nextTick()

      expect(wrapper.vm.$data.rating).toEqual(0)
      expect(wrapper.vm.$data.myRating).toEqual(-1)
      expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
      expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'rating error', type: 'danger' })
    })
  })

  describe('.setHover', () => {
    it('updates value', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.hoverRating).toEqual(-1)
      wrapper.vm.setHover(3)
      expect(wrapper.vm.$data.hoverRating).toEqual(3)
    })
  })

  describe('.clearHover', () => {
    it('starts a timer', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.clearHoverTimer).toBeNull()
      wrapper.vm.clearHover()
      expect(wrapper.vm.$data.clearHoverTimer).not.toBeNull()
    })
  })

  describe('.rate', () => {
    it('posts rating', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      EventBus.$emit = jest.fn()
      Axios.post.mockClear()
      Axios.post.mockResolvedValue({ data: { rating: 4, my_rating: 5, message: 'success msg' } })

      wrapper.vm.rate(5)
      await wrapper.vm.$nextTick()

      expect(Axios.post.mock.calls.length).toBe(1)
      expect(Axios.post.mock.calls[0][0]).toBe('/show/1/rate.json')
      expect(Axios.post.mock.calls[0][1]).toEqual({ rating: { value: 5 } })
      expect(wrapper.vm.$data.rating).toEqual(4)
      expect(wrapper.vm.$data.myRating).toEqual(5)
      expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
      expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'success msg', type: 'success' })
    })

    it('handles simple errors', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      EventBus.$emit = jest.fn()
      Axios.post.mockClear()
      Axios.post.mockRejectedValue('rate failure')

      wrapper.vm.rate(5)
      await wrapper.vm.$nextTick()

      expect(wrapper.vm.$data.rating).toEqual(3)
      expect(wrapper.vm.$data.myRating).toEqual(-1)
      expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
      expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'rate failure', type: 'danger' })
    })

    it('handles list of errors', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      EventBus.$emit = jest.fn()
      Axios.post.mockClear()
      Axios.post.mockRejectedValue({ response: { data: ['error1', 'error2'] } })

      wrapper.vm.rate(5)
      await wrapper.vm.$nextTick()

      expect(wrapper.vm.$data.rating).toEqual(3)
      expect(wrapper.vm.$data.myRating).toEqual(-1)
      expect(EventBus.$emit.mock.calls.length).toBe(2)
      expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
      expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'error1', type: 'danger' })
      expect(EventBus.$emit.mock.calls[1][0]).toEqual('alert')
      expect(EventBus.$emit.mock.calls[1][1]).toEqual({ message: 'error2', type: 'danger' })
    })

    it('handles JSON errors', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      EventBus.$emit = jest.fn()
      Axios.post.mockClear()
      Axios.post.mockRejectedValue({ response: { data: { error: 'my_error', message: 'error message' } } })

      wrapper.vm.rate(5)
      await wrapper.vm.$nextTick()

      expect(wrapper.vm.$data.rating).toEqual(3)
      expect(wrapper.vm.$data.myRating).toEqual(-1)
      expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
      expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'error message', type: 'danger' })
    })
  })
})
