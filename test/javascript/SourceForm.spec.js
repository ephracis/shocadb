import { shallowMount } from '@vue/test-utils'
import SourceForm from 'SourceForm.vue'

import axios from 'axios'
jest.mock('axios')

function mountComponent(props, options) {
  if (!props) {
    props = {
      sourceJson: {
        kind: 'spotify'
      }
    }
  }

  if (!options) {
    options = { propsData: props, stubs: ['font-awesome-icon'] }
  }

  return shallowMount(SourceForm, options)
}

describe('SourceForm', () => {
  it('mounts properly', () => {
    const props = { sourceJson: { kind: 'spotify' } }
    const wrapper = mountComponent(props)
    expect(wrapper.vm.$data.source).toEqual({ kind: 'spotify' })
    expect(wrapper.vm.$data.persisted).toEqual(false)
  })

  it('mounts with cancelUrl', () => {
    const props = { sourceJson: { kind: 'spotify' }, cancelUrl: '/show/1' }
    const wrapper = mountComponent(props)
    expect(wrapper.vm.$data.url.cancel).toEqual('/show/1')
  })

  it('mounts with deleteUrl', () => {
    const props = { sourceJson: { kind: 'spotify' }, deleteUrl: '/sources/1' }
    const wrapper = mountComponent(props)
    expect(wrapper.vm.$data.url.delete).toEqual('/sources/1')
  })

  it('mounts without kind', () => {
    const props = { sourceJson: {} }
    const wrapper = mountComponent(props)
    expect(wrapper.vm.$data.source.kind).toEqual('spotify')
  })

  describe('.saveForm', () => {
    it('sends POST for new source', () => {
      const wrapper = mountComponent()
      axios.post.mockResolvedValue({ data: { foo: 'bar' } })
      wrapper.vm.saveForm()
      expect(axios.post).toHaveBeenCalledWith('/sources.json', { source: { kind: 'spotify' } })
    })

    it('sends PUT for existing source', () => {
      const props = {
        sourceJson: { kind: 'spotify' },
        updateUrl: '/sources/1.json'
      }
      const wrapper = mountComponent(props)
      axios.put.mockResolvedValue({ data: { foo: 'bar' } })
      wrapper.vm.saveForm()
      expect(axios.put).toHaveBeenCalledWith('/sources/1.json', { source: { kind: 'spotify' } })
    })
  })

  describe('.defaultForeignID', () => {
    it('returns empty string when show is missing', () => {
      const wrapper = mountComponent()
      expect(wrapper.vm.defaultForeignID).toEqual('')
    })

    it('returns empty string when show is missing field', () => {
      const props = {
        sourceJson: { kind: 'spotify' },
        showJson: {}
      }
      const wrapper = mountComponent(props)
      expect(wrapper.vm.defaultForeignID).toEqual('')
    })

    it('returns corresponding field for show', () => {
      const props = {
        sourceJson: { kind: 'spotify' },
        showJson: { spotify: 'test-id'}
      }
      const wrapper = mountComponent(props)
      expect(wrapper.vm.defaultForeignID).toEqual('test-id')
    })
  })

  describe('.saveFormError', () => {
    it('sets error when complex data', () => {
      const wrapper = mountComponent()
      wrapper.vm.saveFormError({
        response: { data: { kind: ['some error'] } }
      })
      expect(wrapper.vm.$data.formErrors).toEqual({ kind: ['some error'] })
    })

    it('sets error when simple string', () => {
      const wrapper = mountComponent()
      wrapper.vm.saveFormError('some error')
      expect(wrapper.vm.$data.formErrors).toEqual({ message: 'some error' })
    })
  })

  describe('.fetch', () => {
    it('sends GET request', () => {
      const props = {
        sourceJson: { kind: 'spotify' },
        fetchUrl: '/sources/1/fetch.json'
      }
      const wrapper = mountComponent(props)
      axios.get.mockResolvedValue({ episodes: [] })
      wrapper.vm.fetch()
      expect(wrapper.vm.$data.fetching).toBe(true)
      expect(axios.get).toHaveBeenCalledWith('/sources/1/fetch.json')
    })
  })

  describe('.fetchError', () => {
    it('sets error when complex data', () => {
      const wrapper = mountComponent()
      wrapper.vm.fetchError({
        response: { data: { kind: ['some error'] } }
      })
      expect(wrapper.vm.$data.fetchErrors).toEqual({ kind: ['some error'] })
    })

    it('sets error when simple string', () => {
      const wrapper = mountComponent()
      wrapper.vm.fetchError('some error')
      expect(wrapper.vm.$data.fetchErrors).toEqual({ message: 'some error' })
    })
  })

  describe('.removeEpisode', () => {
    it('removes episode from list', () => {
      const wrapper = mountComponent()
      wrapper.vm.$data.fetchResult = { episodes: [{ id: 1 }, { id: 2 }, { id: 3 }] }
      wrapper.vm.removeEpisode(1)
      expect(wrapper.vm.$data.fetchResult.episodes.length).toBe(2)
      expect(wrapper.vm.$data.fetchResult.episodes[0].id).toBe(1)
      expect(wrapper.vm.$data.fetchResult.episodes[1].id).toBe(3)
    })
  })

  describe('.generate', () => {
    it('sends POST request', () => {
      const props = {
        sourceJson: { kind: 'spotify' },
        generateUrl: '/sources/1/generate.json'
      }
      const wrapper = mountComponent(props)
      window.confirm = jest.fn(() => true)
      axios.post.mockResolvedValue({})
      wrapper.vm.$data.fetchResult = { episodes: [{ id: 1 }, { id: 2 }, { id: 3 }] }
      wrapper.vm.generate()
      expect(window.confirm).toBeCalled()
      expect(wrapper.vm.$data.generating).toBe(true)
      expect(axios.post).toHaveBeenCalledWith('/sources/1/generate.json', { episodes: [{ id: 1 }, { id: 2 }, { id: 3 }] })
    })
  })

  describe('.generateError', () => {
    it('sets error when complex data', () => {
      const wrapper = mountComponent()
      wrapper.vm.generateError({
        response: { data: { kind: ['some error'] } }
      })
      expect(wrapper.vm.$data.fetchErrors).toEqual({ kind: ['some error'] })
    })

    it('sets error when simple string', () => {
      const wrapper = mountComponent()
      wrapper.vm.fetchError('some error')
      expect(wrapper.vm.$data.fetchErrors).toEqual({ message: 'some error' })
    })
  })

  describe('.generateError', () => {
    it('sets error when complex data', () => {
      const wrapper = mountComponent()
      wrapper.vm.generateError({
        response: { data: { kind: ['some error'] } }
      })
      expect(wrapper.vm.$data.fetchErrors).toEqual({ kind: ['some error'] })
    })

    it('sets error when simple string', () => {
      const wrapper = mountComponent()
      wrapper.vm.generateError('some error')
      expect(wrapper.vm.$data.fetchErrors).toEqual({ message: 'some error' })
    })
  })

  describe('.printErrors', () => {
    it('combines list of errors', () => {
      const wrapper = mountComponent()
      const errors = wrapper.vm.printErrors('kind', ['error1', 'error2'])
      expect(errors).toEqual('kind error1.<br/>kind error2.')
    })
  })
})
