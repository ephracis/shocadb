import { shallowMount } from '@vue/test-utils'
import PersonList from 'PersonList.vue'

import axios from 'axios'
jest.mock('axios')

import EventBus from 'eventBus'
jest.mock('eventBus')

global.gtag = jest.fn()

function mountComponent(response, options) {

  // mock axios request
  const defaultResponse = [
    { name: 'Person 1', id: 1, voted: false },
    { name: 'Person 2', id: 2, voted: false }
  ]
  const resp = { data: response || defaultResponse }
  axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  if (!options) {
    options = { propsData: { url: 'http://example.com' }, stubs: ['font-awesome-icon', 'lists-people'] }
  }

  return shallowMount(PersonList, options)
}

describe('PersonList', () => {
  it('has a created hook', () => {
    expect(typeof PersonList.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof PersonList.data).toBe('function')
    const defaultData = PersonList.data()
    expect(defaultData.loading).toEqual(true)
    expect(defaultData.people).toEqual([])
    expect(defaultData.error).toEqual('')
  })

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.$data.people.length).toEqual(0)
  })

  it('displays error message', async () => {
    const wrapper = mountComponent(null, { propsData: { url: '' }, stubs: ['font-awesome-icon', 'lists-people'] })
    await wrapper.vm.$nextTick()
    wrapper.vm.$data.error = 'test error'
    await wrapper.vm.$nextTick()
    expect(wrapper.text()).toContain('test error')
  })

  describe('getting people list', () => {

    it('sets people from URL', async () => {
      const people = [ { id: 1, name: 'Test Person', voted: false } ]
      const url = 'https://example.com/people.json'
      const wrapper = mountComponent(people, { propsData: { url: url }, stubs: ['font-awesome-icon', 'lists-people'] })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.people.length).toEqual(1)
      expect(wrapper.vm.$data.people[0]['id']).toEqual(1)
      expect(wrapper.vm.$data.people[0]['name']).toEqual('Test Person')
    })

    it('handles API errors', async () => {
      axios.get.mockRejectedValue('my error')
      console.error = jest.fn()

      const url = 'https://example.com/people.json'
      const wrapper = shallowMount(PersonList, { propsData: { url: url }, stubs: ['font-awesome-icon', 'lists-people'] })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.people.length).toEqual(0)
      expect(wrapper.vm.$data.error).toEqual('Error fetching guests: my error')
    })
  })
})
