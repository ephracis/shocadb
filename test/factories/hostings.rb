# frozen_string_literal: true

FactoryBot.define do
  factory :hosting do
    person { create(:person) }
    show { create(:show) }
  end
end
