# frozen_string_literal: true

FactoryBot.define do
  factory :source do
    show { create(:show) }
    foreign_id { 'my-foreign-id' }
    kind { :spotify }

    factory :spotify_source do
      kind { :spotify }
    end

    factory :youtube_source do
      kind { :youtube }
    end
  end
end
