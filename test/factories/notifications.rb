# frozen_string_literal: true

FactoryBot.define do
  factory :notification do
    user { create(:user) }
    title { 'MyString' }
    content { 'MyText' }
    read { false }
    image { 'http://myimage.jpg' }

    factory :read_notification do
      read { true }
    end

    factory :unread_notification do
      read { false }
    end
  end
end
