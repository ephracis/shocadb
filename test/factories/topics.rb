# frozen_string_literal: true

FactoryBot.define do
  factory :topic do
    sequence(:slug) { |n| "my_topic_#{n}" }
    sequence(:name) { |n| "My Topic #{n}" }
  end
end
