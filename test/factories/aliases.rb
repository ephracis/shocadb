# frozen_string_literal: true

FactoryBot.define do
  factory :alias do
    sequence(:name) { |n| "My alias #{n}" }
    aliasable { create(:person) }
  end
end
