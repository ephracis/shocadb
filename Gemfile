source 'https://rubygems.org'
ruby '~> 3.0.0'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails'
# Use Puma as the app server
gem 'puma'
# Use SCSS for stylesheets
gem 'sass-rails'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'
# Use Redis adapter to run Action Cable in production
gem 'redis'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
gem 'image_processing'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', require: false

group :production do
  # Use PosgreSQL as the database for Active Record
  gem 'pg'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger
  # console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  # Use sqlite3 as the database in test and dev
  gem 'sqlite3'
  # Use FactoryBot to generate fixtures
  gem 'factory_bot_rails'
end

group :development do
  gem 'listen'
  # Access an interactive console on exception pages or by calling 'console'
  # anywhere in the code.
  gem 'web-console'
  # Spring speeds up development by keeping your application running in the
  # background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen'

  ###
  # Customizations from vanilla Rails
  #

  # Generate fake data
  gem 'faker'

  # Run Rails with automatic reloads
  gem 'foreman'

  # View emails inside browser
  gem 'letter_opener'

  # VSCode debugger
  gem 'ruby-debug-ide'
  gem 'debase', '= 0.2.5.beta2'

  # Benchmark
  gem 'derailed'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara'
  gem 'haml_lint'
  gem 'mocha'
  gem 'rubocop'
  gem 'rubocop-rails'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
  # Test coverage
  gem 'simplecov'
  gem 'simplecov-cobertura'
end

###
# Customizations from vanilla Rails
#

# HAML is much cleaner than HTML
gem 'haml'

# Select box in views with every country
gem 'country_select'

# Authentication and user management
gem 'devise'

# Authorization
gem 'pundit'

# Anti bot
gem 'recaptcha'

# Authentication using Google Accounts
gem 'omniauth', '~> 1.9.1'
gem 'omniauth-google-oauth2'

# Generate sitemap for search engines, and upload it to AWS
gem 'sitemap_generator'
gem 'google-cloud-storage'

# Automatically detect locale in request
gem 'http_accept_language'

# Language metrics in Heroku
gem 'barnes'

# Monitor performance in New Relic
gem 'newrelic_rpm'

# Pagination
gem 'pagy'

# Use Redis for cache
#gem 'redis-store'

# User achievements and points
gem 'merit'

# Collect crash reports
gem 'sentry-ruby'
gem 'sentry-rails'
