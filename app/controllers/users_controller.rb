# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :authenticate_user!

  def show
    @user = User.find(params[:id])
    render layout: 'sidebar'
  end

  def edit_password
    @user = current_user
    authorize @user, :update?
  end

  def achievements
    @my_badges = current_user.earned_badges
    @user = current_user
    @badges = []
    Merit::Badge.all.group_by { |b| b.name.itself }.each do |group|
      badges = group[1]
      @badges << if badges.length == 1
                   { badge: badges[0], earned: current_user.earned_badge?(badges[0].name) }
                 else
                   { badge: badges[0], levels: badges.length, earned: current_user.earned_badge?(badges[0].name) }
                 end
    end
    render layout: 'user'
  end

  def update_password
    @user = current_user
    authorize @user, :update?
    if update_user(@user, user_params)
      # Sign in the user by passing validation in case their password changed
      bypass_sign_in(@user)
      redirect_to root_path
    else
      render :edit_password
    end
  end

  private

  def user_params
    params.require(:user).permit(:current_password, :password, :password_confirmation)
  end

  def update_user(user, user_params)
    if user.no_password
      user.update(user_params) && user.update(no_password: false)
    else
      user.update_with_password(user_params)
    end
  end
end
