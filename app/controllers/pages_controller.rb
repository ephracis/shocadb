# frozen_string_literal: true

class PagesController < ApplicationController
  def welcome
    set_settings
  end

  def privacy; end

  def terms; end

  def sitemap
    redirect_to 'https://storage.googleapis.com/sitemap.shocadb.com/sitemap.xml.gz'
  end

  def not_found
    render status: 404
  end

  def internal_server_error
    render status: 500
  end
end
