# frozen_string_literal: true

class RegistrationsController < Devise::RegistrationsController
  prepend_before_action :check_captcha, only: [:create]

  def create
    log_event 'sign_up', { method: 'Local' }
    super
  end

  def update
    @registration = @user = resource # Needed for Merit
    super
  end

  def check_captcha
    return if verify_recaptcha(action: 'signup')

    self.resource = resource_class.new sign_up_params
    resource.validate # Look for any other validation errors besides reCAPTCHA
    set_minimum_password_length

    respond_with_navigational(resource) do
      flash[:recaptcha_error] = 'reCAPTCHA verification failed, please try again.' unless flash[:recaptcha_error]
      flash.discard(:recaptcha_error) # We need to discard flash to avoid showing it on the next page reload
      render :new
    end
  end

  protected

  def after_update_path_for(_resource)
    edit_user_registration_path
  end

  def update_resource(resource, params)
    resource.update_without_password(params)
  end
end
