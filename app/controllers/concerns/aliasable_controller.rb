# frozen_string_literal: true

module AliasableController
  extend ActiveSupport::Concern

  def update_aliases(resource, parameters)
    return true unless parameters[:aliases].present?

    authorize resource.aliases.new, :create?

    # remove old aliases
    resource.aliases.each do |resource_alias|
      return false if !resource_alias.name.in?(parameters[:aliases]) && !resource_alias.destroy
    end

    # create new aliases
    parameters[:aliases].each do |alias_name|
      return false unless resource.aliases.where(name: alias_name).first_or_create!
    end

    parameters.extract! :aliases
    true
  end
end
