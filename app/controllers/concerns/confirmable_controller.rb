# frozen_string_literal: true

module ConfirmableController
  extend ActiveSupport::Concern

  def update_resource_confirmation(resource, parameters)
    return true if parameters[:confirmed].blank?

    authorize resource, :confirm?

    confirm_value = parameters[:confirmed].to_i.zero?
    parameters.extract! :confirmed

    if confirm_value
      resource.unconfirm
    else
      resource.confirm(current_user)
    end
  end
end
