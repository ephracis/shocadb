# frozen_string_literal: true

module BlockableController
  extend ActiveSupport::Concern

  def update_resource_blockade(resource, parameters)
    return true if parameters[:blocked].blank?

    authorize resource, :block?

    block_value = parameters[:blocked].to_i.zero?
    parameters.extract! :blocked

    if block_value
      resource.unblock
    else
      resource.block(current_user)
    end
  end
end
