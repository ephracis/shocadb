# frozen_string_literal: true

class NotificationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_notification, only: %i[destroy]

  # GET /notifications
  # GET /notifications.json
  def index
    @notifications = current_user.notifications.order(created_at: :desc)
  end

  # PATCH /notifications/read.json
  def read
    current_user.notifications.update_all(read: true)
    @notifications = current_user.notifications.order(created_at: :desc)
    render :index
  end

  # DELETE /notifications/1
  # DELETE /notifications/1.json
  def destroy
    authorize @notification
    @notification.destroy
    respond_to do |format|
      format.html { redirect_to notifications_url, notice: t('notifications.notice.destroy') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_notification
    @notification = Notification.find(params[:id])
  end
end
