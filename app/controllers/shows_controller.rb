# frozen_string_literal: true

class ShowsController < ApplicationController
  include ConfirmableController
  include BlockableController
  include TopicableController
  include ImageableController
  include RateableController
  include EditableController

  before_action :authenticate_user!, except: %i[index show rating]
  before_action :set_resource, only: %i[show edit update destroy edits]

  # GET /shows
  # GET /shows.json
  def index
    @shows = if params[:q].present?
               Show.search(params[:q])
             else
               Show.top
             end
    @shows = @shows.unblocked(current_user).confirmed(current_user).limit(params[:limit] || 20)
  end

  # GET /shows/1
  # GET /shows/1.json
  def show
    @edits = edits_for @show
    render layout: 'sidebar'
  end

  # GET /shows/new
  def new
    @show = Show.new
    authorize @show, :create?
  end

  # GET /shows/1/edit
  def edit
    authorize @show, :update?
    save_redirect_path
    @cancel_path = saved_redirect_path || show_path(@show)
  end

  # POST /shows
  # POST /shows.json
  def create
    @show = Show.new
    authorize @show

    respond_to do |format|
      if create_show(@show)
        flash[:notice] = 'Show was successfully created.'
        format.html { redirect_to @show }
        format.json { render :show, status: :created, location: @show }
        format.js { render js: "window.location='#{url_for(@show)}'" }
      else
        format.html { render :new }
        format.json { render json: @show.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /shows/1
  # PATCH/PUT /shows/1.json
  def update
    authorize @show, :edit?
    respond_to do |format|
      if update_show
        flash[:notice] = successful_update_notice
        format.html { redirect_to after_update_path }
        format.json { render :show, status: :ok, location: @show }
        format.js { render js: "window.location='#{after_update_path}'" }
      else
        format.html { render :edit }
        format.json { render json: @show.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /shows/1
  # DELETE /shows/1.json
  def destroy
    authorize @show
    @show.destroy
    respond_to do |format|
      format.html { redirect_to shows_url, notice: 'Show was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_resource
    @resource = @show = Show.find_by!(slug: params[:id])
  rescue ActiveRecord::RecordNotFound
    resource = Slug.find_by!(name: params[:id], slugable_type: 'Show').hit!.slugable
    redirect_to id: resource.to_param, status: :moved_permanently
  end

  # Only allow a list of trusted parameters through.
  def show_params
    params.require(:show).permit(
      :slug, :title, :youtube, :twitter, :facebook, :rumble, :parler, :website, :apple_podcast, :spotify, :description,
      :image, :confirmed, :blocked, hosts: [], topics: []
    )
  end

  # Create show model.
  def create_show(show)
    parameters = show_params
    return false unless update_resource_confirmation show, parameters
    return false unless update_resource_blockade show, parameters
    return false unless attach_image show, parameters

    show.attributes = parameters.except(:topics)
    show.created_by = current_user
    show.generate_slug unless parameters[:slug]

    return false unless show.save
    return false unless update_topics show, parameters

    true
  end

  # Update show model.
  def update_show
    parameters = show_params # save result from method so it can be modified through the update calls
    return false unless update_resource_confirmation @show, parameters
    return false unless update_resource_blockade @show, parameters
    return false unless update_hosts @show, parameters
    return false unless update_topics @show, parameters
    return false unless attach_image @show, parameters
    return false unless create_edits @show, parameters

    true
  end

  def update_hosts(show, parameters)
    return true unless parameters[:hosts].present?
    return false unless remove_hosts(show, parameters)
    return false unless add_hosts(show, parameters)

    parameters.extract! :hosts
    true
  end

  def remove_hosts(show, parameters)
    show.hostings.each do |hosting|
      id_present = hosting.person.id.in?(parameters[:hosts])
      name_present = hosting.person.name.in?(parameters[:hosts])
      next if id_present || name_present

      authorize hosting, :destroy?
      return false unless hosting.destroy
    end
  end

  def add_hosts(show, parameters)
    parameters[:hosts].each do |host|
      person = Person.find_by(id: host)
      person ||= Person.find_by_alias(host)
      person ||= create_new_person(host)
      hosting = show.hostings.find_by(person: person)
      next if hosting

      hosting = show.hostings.new(person: person)
      authorize hosting, :create?
      return false unless hosting.save
    end
  end

  def after_update_path
    return saved_redirect_path if saved_redirect_path.present?

    if show_params.keys.length == 1 && show_params[:confirmed]
      admin_unconfirmed_path
    else
      url_for(@show)
    end
  end
end
