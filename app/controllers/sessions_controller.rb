# frozen_string_literal: true

class SessionsController < Devise::SessionsController
  prepend_before_action :check_captcha, only: [:create]

  def create
    log_event 'login', { method: 'Local' }
    super
  end

  def check_captcha
    return if verify_recaptcha(action: 'login', minimum: 1)

    self.resource = resource_class.new sign_in_params

    score = recaptcha_reply.present? ? recaptcha_reply['score'] : 'N/A'
    Rails.logger.warn("User #{sign_in_params[:login]} was denied login because of a recaptcha score of #{score}")

    respond_with_navigational(resource) do
      flash[:recaptcha_error] = 'reCAPTCHA verification failed, please try again.' unless flash[:recaptcha_error]
      flash.discard(:recaptcha_error)
      render :new
    end
  end
end
