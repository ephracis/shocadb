# frozen_string_literal: true

class EditsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_resource

  # GET /edits/1/edit
  def edit
    authorize @edit, :update?
  end

  # PATCH/PUT /edits/1
  def update
    authorize @edit
    respond_to do |format|
      if @edit.update(edit_params)
        format.html { redirect_to edit_edit_path(@edit), notice: t('edit.notice.update') }
        format.js { render js: "window.location='#{edit_edit_path(@edit)}'" }
      else
        format.html { render :edit }
        format.js
      end
    end
  end

  # PATCH /edits/1/apply
  def apply
    authorize @edit
    respond_to do |format|
      if @edit.apply!(current_user)
        format.html { redirect_to redirect_path, notice: t('edit.notice.apply') }
      else
        format.html { redirect_to redirect_path, alert: @edit.errors.full_messages.join }
      end
    end
  end

  # PATCH /edits/1/discard
  def discard
    authorize @edit
    respond_to do |format|
      if @edit.discard!(current_user)
        format.html { redirect_to redirect_path, notice: t('edit.notice.discard') }
      else
        format.html { redirect_to redirect_path, alert: @edit.errors.full_messages.join }
      end
    end
  end

  # DELETE /edit/1
  def destroy
    authorize @edit
    @edit.destroy
    respond_to do |format|
      format.html { redirect_to redirect_path, notice: t('edit.notice.destroy') }
    end
  end

  private

  def set_resource
    @resource = @edit = Edit.find(params[:id])
  end

  def edit_params
    params.require(:edit).permit(:value)
  end

  def redirect_path
    request.referer.present? ? request.referer : url_for(@edit.editable)
  end
end
