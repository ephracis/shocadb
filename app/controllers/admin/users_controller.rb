# frozen_string_literal: true

module Admin
  class UsersController < ApplicationController
    before_action :authenticate_user!, :authorize_root!
    before_action :set_user, only: %i[show edit update destroy]
    layout 'admin'

    # GET /admin/users
    # GET /admin/users.json
    def index
      @users = User.all
    end

    # GET /admin/users/1
    # GET /admin/users/1.json
    def show
      authorize @user, :update?
    end

    # PATCH/PUT /admin/users/1
    # PATCH/PUT /admin/users/1.json
    def update
      authorize @user
      respond_to do |format|
        if update_user
          format.html { redirect_to admin_user_path(@user), notice: 'User was successfully updated.' }
          format.json { render :show, status: :ok, location: admin_user_path(@user) }
          format.js { render js: "window.location='#{admin_user_path(@user)}'" }
        else
          format.html { render :show }
          format.json { render json: @user.errors, status: :unprocessable_entity }
          format.js
        end
      end
    end

    private

    # Update user model.
    def update_user
      success = true
      p = user_params

      # update confirmation
      if p[:confirmed].to_i.zero?
        success = false unless @user.update(confirmed_at: nil)
      else
        success = false unless @user.confirm
      end
      p.extract! :confirmed

      # update the rest
      success = false unless @user.update(p)

      success
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:email, :username, :locked, :confirmed, role_ids: [])
    end
  end
end
