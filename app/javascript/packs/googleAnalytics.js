export default {
  event: function (name, params) {
    if (global.gtag) {
      global.gtag('event', name, params)
    }
  }
}
