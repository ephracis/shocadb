# frozen_string_literal: true

class Edit < ApplicationRecord
  belongs_to :editable, polymorphic: true
  belongs_to :created_by, class_name: 'User'
  belongs_to :discarded_by, class_name: 'User', optional: true
  belongs_to :applied_by, class_name: 'User', optional: true
  belongs_to :previous, class_name: 'Edit', optional: true
  has_one :next, class_name: 'Edit', foreign_key: :previous_id

  scope :applied, -> { where(applied: true) }
  scope :discarded, -> { where(discarded: true) }
  scope :pending, -> { where(discarded: false, applied: false) }

  validates :value, length: { minimum: 2 }
  validates :name, presence: true,
                   uniqueness: { scope: %i[editable_type editable_id created_by_id] }

  def pending?
    !applied? && !discarded?
  end

  def discard!(user)
    update(discarded: true, discarded_by: user, discarded_at: DateTime.now)
  end

  def apply!(user)
    editable.ensure_edit_for name, user
    self.previous = editable.last_applied_edit
    self.applied_by = user
    self.applied_at = DateTime.now
    self.applied = true
    return false unless save

    editable.update_attribute(name, value)
    editable.pending_edits.each do |edit|
      edit.discard!(user) if edit.name == name && edit.value == value
    end

    true
  end
end
