# frozen_string_literal: true

class Rating < ApplicationRecord
  belongs_to :user
  belongs_to :rateable, polymorphic: true

  validates :value, numericality: { only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 5 }
  validates :user, :rateable, presence: true
  validates :user_id, uniqueness: { scope: %i[rateable_id rateable_type] }
end
