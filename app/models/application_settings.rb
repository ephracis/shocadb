# frozen_string_literal: true

class ApplicationSettings < ApplicationRecord
  validates :max_votes_per_show, numericality: { greater_than: -2, only_integer: true }
end
