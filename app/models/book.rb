# frozen_string_literal: true

class Book < ApplicationRecord
  include Searchable
  include Confirmable
  include Blockable
  include Slugable
  include Topicable
  include Imageable
  include Rateable

  has_and_belongs_to_many :authors, class_name: 'Person'

  validates :slug, presence: true,
                   uniqueness: { case_sensitive: false },
                   length: { minimum: 2 },
                   format: { with: /\A[a-z0-9_-]+\z/,
                             message: 'can only contain lowercase letters, numbers, underscore and dash' }
  validates :name, presence: true,
                   uniqueness: { case_sensitive: false },
                   length: { minimum: 2 }

  alias_attribute :display_name, :title

  def self.with_name(name)
    where('LOWER(name) = ?', Book.clean_name(name).downcase)
  end

  def to_s
    name
  end

  def external_links?
    amazon.present?
  end

  def amazon_url
    if amazon.starts_with? 'http'
      amazon
    else
      amazon_associate_id = Rails.application.credentials.dig(:amazon, :associate_id)
      "https://amazon.com/dp/#{amazon}?ref=nosim&tag=#{amazon_associate_id}"
    end
  end
end
