# frozen_string_literal: true

# Be sure to restart your server when you modify this file.
#
# +grant_on+ accepts:
# * Nothing (always grants)
# * A block which evaluates to boolean (recieves the object as parameter)
# * A block with a hash composed of methods to run on the target object with
#   expected values (+votes: 5+ for instance).
#
# +grant_on+ can have a +:to+ method name, which called over the target object
# should retrieve the object to badge (could be +:user+, +:self+, +:follower+,
# etc). If it's not defined merit will apply the badge to the user who
# triggered the action (:action_user by default). If it's :itself, it badges
# the created object (new user for instance).
#
# The :temporary option indicates that if the condition doesn't hold but the
# badge is granted, then it's removed. It's false by default (badges are kept
# forever).

module Merit
  class BadgeRules
    include Merit::BadgeRulesMethods

    def initialize
      grant_on 'confirmations#show', badge: 'registrator', level: (2050 - DateTime.now.year), model_name: 'User',
                                     to: :itself

      [1, 2, 10, 25, 50, 75, 100, 150, 200, 500].each_with_index do |count, index|
        grant_on 'votes#create', badge: 'voter', level: index + 1 do |vote|
          vote.user && vote.user.votes.count >= count
        end
        grant_on 'shows#update', badge: 'show-creator', level: index + 1, to: :created_by do |show|
          show.created_by.created_shows.confirmed.count >= count
        end
        grant_on 'episodes#update', badge: 'episode-creator', level: index + 1, to: :created_by do |episode|
          episode.created_by.created_episodes.confirmed.count >= count
        end
        grant_on 'people#update', badge: 'person-creator', level: index + 1, to: :created_by do |person|
          person.created_by.created_people.confirmed.count >= count
        end
        grant_on 'books#update', badge: 'book-creator', level: index + 1, to: :created_by do |book|
          book.created_by.created_books.confirmed.count >= count
        end
        grant_on 'edits#apply', badge: 'editor', level: index + 1, to: :created_by do |edit|
          edit.created_by.edits.applied.count >= count
        end
      end

      grant_on 'registrations#update', badge: 'user-fullname', temporary: true, model_name: 'User' do |user|
        user.fullname.length > 4
      end

      grant_on 'registrations#update', badge: 'user-social', temporary: true, model_name: 'User' do |user|
        user.twitter.present? && user.facebook.present?
      end

      grant_on 'registrations#update', badge: 'user-nation', temporary: true, model_name: 'User' do |user|
        user.country.present? && user.time_zone.present?
      end

      grant_on 'registrations#update', badge: 'user-email', temporary: true, model_name: 'User' do |user|
        user.email.present? && user.email_notifications
      end
    end
  end
end
