# frozen_string_literal: true

module Searchable
  extend ActiveSupport::Concern

  included do
    def search_result_values
      {
        id: id,
        display: name,
        image: image
      }
    end
  end

  class_methods do
    def search(query)
      if query
        where("lower(#{search_field}) LIKE ?", "%#{query.downcase}%")
      else
        all
      end
    end

    def search_field(field = nil)
      @search_field_name = field if field.present?
      @search_field_name || :name
    end
  end
end
