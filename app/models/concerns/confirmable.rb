# frozen_string_literal: true

module Confirmable
  extend ActiveSupport::Concern

  included do
    belongs_to :created_by, class_name: 'User', optional: true
    belongs_to :confirmed_by, class_name: 'User', optional: true

    def self.confirmed(user = nil)
      if user
        where(created_by: user).or(confirmed)
      else
        where.not(confirmed_by: nil)
      end
    end

    def confirmed?
      !confirmed_at.nil?
    end

    def confirm(user)
      update(confirmed_by: user, confirmed_at: DateTime.now)
    end

    def unconfirm
      update(confirmed_by: nil, confirmed_at: nil)
    end

    def auto_confirm!
      auto_confirm
      save!
    end

    def auto_confirm
      self.confirmed_at = DateTime.now

      root_role = Role.where(name: :root).first
      admin_role = Role.where(name: :admin).first

      self.confirmed_by = if root_role
                            root_role.users.first
                          elsif admin_role
                            admin_role.users.first
                          else
                            User.first
                          end
      self.created_by = confirmed_by unless created_by
    end
  end
end
