# frozen_string_literal: true

module Duplicatable
  extend ActiveSupport::Concern

  included do
    # Mark this resource as a duplicate
    #
    # This will remove the current resource and merge its attributes and associations into the other resource.
    def duplicate_of!(original)
      ensure_same_class_as! original
      copy_attributes_to original
      copy_associations_to original

      original.slugs.create(name: slug)
      original.aliases.create(name: send(self.class.aliased_field))

      reload
      destroy!
    end

    private

    def copy_attributes_to(original)
      original.attributes.each do |k, v|
        next if v.present? || self[k].blank?

        original[k] = self[k]
      end
      original.save!
    end

    def ensure_same_class_as!(original)
      return if instance_of?(original.class)

      raise "Resource of type #{self.class} cannot be duplicate of resource of type #{original.class}"
    end

    def copy_association(collection, key, value)
      collection.each { |item| item.update_attribute(key, value) }
    end
  end
end
