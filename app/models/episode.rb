# frozen_string_literal: true

class Episode < ApplicationRecord
  include Searchable
  include Confirmable
  include Blockable
  include Slugable
  include Topicable
  include Imageable
  include Rateable
  include Editable
  include ExternalLinkable
  include Duplicatable
  include Aliasable

  belongs_to :show
  has_many :appearances, dependent: :destroy
  has_many :people, through: :appearances

  validates :slug, presence: true,
                   uniqueness: { case_sensitive: false, scope: :show },
                   length: { minimum: 2 },
                   format: { with: /\A[a-z0-9_-]+\z/,
                             message: 'can only contain lowercase letters, numbers, underscore and dash' }
  validates :title, presence: true,
                    uniqueness: { case_sensitive: false, scope: :show },
                    length: { minimum: 2 }

  search_field :title
  slugged_field :title
  aliased_field :title
  youtube_url_pattern 'https://youtube.com/watch?v=%s'
  spotify_url_pattern 'https://open.spotify.com/episode/%s'

  alias_attribute :display_name, :title

  def search_result_values
    {
      id: id,
      display: title,
      image: image
    }
  end

  def full_title
    "#{show.title} - #{title}"
  end

  def to_s
    title
  end

  def self.with_title(title)
    find_by_alias clean_name(title).downcase
  end

  def self.from_source(source_response)
    validate_source_response! source_response

    source_response['episodes'].each do |episode_json|
      validate_source_episode! episode_json

      episode = with_title(episode_json['title'])

      unless episode
        episode = new(
          title: episode_json['title'].squish,
          description: episode_json['description'],
          aired_at: episode_json['aired_at'].to_datetime,
          generated: true
        )
        episode.attach_image episode_json['image']
        episode.generate_slug
        episode.auto_confirm
        episode.save!
        episode.create_guests_from_list episode_json['guests']
      end

      episode.save_source_id source_response['source'], episode_json
    end
  end

  def save_source_id(source_json, episode_json)
    case source_json['kind']
    when 'youtube'
      update youtube: episode_json['source_id']
    when 'spotify'
      update spotify: episode_json['source_id']
    end
  end

  def self.validate_source_response!(response)
    raise 'Episodes are missing from payload.' unless response && response['episodes'].is_a?(Array)
  end

  def self.validate_source_episode!(episode)
    %w[
      title
      source_id
    ].each do |field|
      raise "Attribute #{field} is missing from episode in payload." unless episode[field]
    end
  end

  def create_guests_from_list(guest_list)
    return unless guest_list.present?

    guest_list.each do |guest_name|
      person = Person.find_by_alias guest_name
      person ||= Person.where(name: guest_name).first_or_create do |p|
        p.generated = true
        p.generate_slug
        p.auto_confirm
      end
      appearances.where(person: person).first_or_create
    end
  end

  private

  def copy_associations_to(original)
    copy_association appearances, :episode, original
    copy_association slugs, :slugable_id, original.id
    copy_association aliases, :aliasable_id, original.id
    copy_association topic_topicables, :topicable_id, original.id
    copy_association ratings, :rateable_id, original.id
    copy_association edits, :editable_id, original.id
    original.image.attach image.blob unless original.image.attached?
  end
end
