# frozen_string_literal: true

json.extract! vote, :id, :value, :person, :show, :created_at, :updated_at
