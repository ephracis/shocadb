# frozen_string_literal: true

json.vote do
  json.extract! @vote, :id, :value, :created_at, :updated_at

  json.person do
    json.partial! 'people/person', person: @vote.person
  end

  if @vote.user
    json.user do
      json.extract! @vote.user, :id, :username, :created_at, :updated_at
    end
  end

  json.show do
    json.partial! 'shows/show', show: @vote.show
  end
end

json.limit do
  if current_user
    count = current_user.unfulfilled_votes_for(@vote.show)
    max = @settings.max_votes_per_show
  else
    count = Vote.where(ip_address: request.remote_ip).size
    max = @settings.max_votes_per_ip
  end
  json.count max - count
  json.max max
  json.message t('vote.notice.limit', count: max - count)
end
