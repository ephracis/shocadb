# frozen_string_literal: true

json.extract! episode, :id, :slug, :title, :display_name, :youtube, :created_at, :updated_at
json.aired_at_human display_date(episode.aired_at, :long)
json.description sanitize(episode.description.to_s.gsub("\n", '<br/>'))
json.image episode_image(episode)

json.url show_episode_url(episode.show, episode, format: :json)
json.view_url show_episode_url(episode.show, episode)
json.destroy_url show_episode_url(episode.show, episode) if policy(episode).destroy?
json.edit_url edit_show_episode_url(episode.show, episode) if policy(episode).edit?
json.merge_url merge_show_episode_url(episode.show, episode) if policy(episode).merge?
json.report_url new_report_path_for(episode) if policy(Report).create?

json.show do
  json.extract! episode.show, :id, :slug, :title
  json.url show_url(episode.show, format: :json)
  json.view_url show_url(episode.show)
end
json.people episode.people do |person|
  json.extract! person, :id, :slug, :name
  json.url person_url(person, format: :json)
  json.view_url person_url(person)
end
