# frozen_string_literal: true

json.array! @episode.people, partial: 'people/person', as: :person
