# frozen_string_literal: true

json.extract! person, :id, :name, :display_name, :slug, :confirmed?, :blocked?, :created_at, :updated_at
json.description sanitize(person.description.to_s.gsub("\n", '<br/>'))
json.voted person.any_votes_for?(show: @show, user: current_user || request.remote_ip)
json.favorited person.favorited_by?(current_user)
json.image person_image(person)

json.url person_url(person, format: :json)
json.html_url person_url(person, format: nil)
json.show_url show_person_url(@show, person, format: nil) if @show
json.view_url person_url(person)
json.destroy_url person_url(person) if policy(person).destroy?
json.edit_url edit_person_url(person) if policy(person).edit?
json.report_url new_report_path_for(person) if policy(Report).create?

json.show_count person.shows.count
json.episode_count person.episodes.count
json.vote_count person.vote_count
