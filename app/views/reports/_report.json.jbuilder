# frozen_string_literal: true

json.extract! report, :id, :comment, :user_id, :category, :reportable_id, :reportable_type, :read, :created_at,
              :updated_at
json.url report_url(report, format: :json)
