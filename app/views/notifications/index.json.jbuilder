# frozen_string_literal: true

json.array! @notifications do |notification|
  json.extract! notification, :id, :title, :content, :image, :read, :created_at
  json.created_ago distance_of_time_in_words(notification.created_at, DateTime.now)
end
