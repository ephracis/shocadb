# frozen_string_literal: true

json.extract! source, :id, :kind, :foreign_id, :parse_title_for_guests, :guests_pattern, :split_pattern, :scheduled,
              :created_at, :updated_at
json.display source.kind.capitalize
json.url show_source_url(source.show, source, format: :json)
json.edit_url edit_show_source_url(source.show, source)
json.fetch_url fetch_show_source_url(source.show, source, format: :json)
json.generate_url generate_show_source_url(source.show, source, format: :json)
json.delete_url show_source_url(source.show, source)
