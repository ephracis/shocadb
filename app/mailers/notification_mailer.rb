# frozen_string_literal: true

class NotificationMailer < ApplicationMailer
  default from: 'notifications@shocadb.com'

  def appearance
    @notification = params[:notification]
    mail(to: @notification.user.email, subject: @notification.title)
  end
end
