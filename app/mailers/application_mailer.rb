# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'hello@shocadb.com'
  layout 'mailer'
end
