# frozen_string_literal: true

class ReportPolicy < ApplicationPolicy
  def index?
    return false unless user

    user.roles?(:admin, :root)
  end

  def create?
    return false unless user

    !user.role?(:blocked)
  end

  def update?
    return false unless user

    user.roles?(:admin, :root)
  end

  def destroy?
    return false unless user

    user.roles?(:admin, :root)
  end
end
