# frozen_string_literal: true

class RolePolicy < ApplicationPolicy
  def create?
    user&.role?(:root)
  end

  def update?
    user&.role?(:root)
  end

  def destroy?
    user&.role?(:root)
  end
end
