# frozen_string_literal: true

class EditPolicy < ApplicationPolicy
  def update?
    user && (record.created_by == user || user.roles?(:admin, :root))
  end

  def destroy?
    user && (record.created_by == user || user.roles?(:admin, :root))
  end

  def apply?
    user&.roles?(:admin, :root)
  end

  def discard?
    user&.roles?(:admin, :root)
  end
end
