# frozen_string_literal: true

class NotificationPolicy < ApplicationPolicy
  def create?
    user && !user.role?(:blocked)
  end

  def update?
    return false unless user

    user == record.user || user.roles?(:admin, :root)
  end

  def destroy?
    return false unless user

    user == record.user || user.roles?(:admin, :root)
  end
end
