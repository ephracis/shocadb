# frozen_string_literal: true

module ReportHelper
  # Generate a link to report a resource.
  def link_to_report(resource, options = {})
    return unless policy(Report).create?

    options = {
      class: 'btn btn-sm btn-outline-danger'
    }.merge(options)

    link_to new_report_path_for(resource), options do
      concat content_tag(:i, '', class: 'fas fa-fw fa-flag mr-1')
      concat content_tag(:span, t('button.report'))
    end
  end

  def new_report_path_for(resource)
    new_report_path(report: { reportable_id: resource.id, reportable_type: resource.class })
  end

  # Create shortcut for URL to report so we do not have to explicitly tell Rails to use /admin
  def report_url(report, opts = {})
    admin_report_url(report, opts)
  end

  def report_path(report, opts = {})
    admin_report_path(report, opts)
  end
end
