# frozen_string_literal: true

module EpisodeHelper
  def episode_meta_description(episode)
    "Check out who appeared on #{episode.title} on the podcast show #{episode.show.title}."
  end

  def episode_meta_keywords(episode)
    [
      'podcast',
      'show',
      'guests',
      episode.title
    ].join(',')
  end

  def episode_image(episode)
    if episode.image.attached?
      url_for(episode.image)
    elsif episode.show.image.attached?
      url_for(episode.show.image)
    else
      image_url('placeholder-show.svg')
    end
  end
end
