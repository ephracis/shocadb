# frozen_string_literal: true

namespace :data do
  namespace :migrate do
    desc 'Migrate pseudonyms to aliases'
    task pseudonyms_to_aliases: :environment do
      pseudonyms = Pseudonym.all
      puts "Going to migrate #{pseudonyms.count} pseudonyms"
      ActiveRecord::Base.transaction do
        pseudonyms.each do |pseudonym|
          Alias.find_or_create_by!(name: pseudonym.name, aliasable: pseudonym.person)
          print '.'
        end
      end
      puts ' Done!'
    end
  end

  task migrate: ['migrate:pseudonyms_to_aliases']
end
