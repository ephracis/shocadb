# frozen_string_literal: true

namespace :faker do
  desc 'Generate fake data'
  task generate: :environment do
    unless Rails.env.development?
      puts 'This can only be run in the development environment. Aborting!'
      return
    end

    ActionMailer::Base.perform_deliveries = false
    puts ''

    puts 'Generate admin'
    puts "  #{generate_admin.email}"
    puts ''

    generate_resources :users, 15, :email
    generate_resources :shows, 15, :title
    generate_resources :people, 25, :name

    puts 'Generate votes'
    (0..50).each do |_n|
      vote = generate_vote
      puts "  #{vote.user.email} wants #{vote.person.name} on #{vote.show.title}"
    end
    puts ''
  end
end

task faker: ['db:reset', 'faker:generate']

def generate_resources(name, number, attribute)
  puts "Generate #{name}"
  (0..number).each do |_n|
    method_name = "generate_#{name.to_s.singularize}"
    puts "  #{send(method_name).send(attribute)}"
  end
  puts ''
end

def generate_admin
  admin = User.create!(
    email: 'admin@mail.com',
    username: 'admin',
    fullname: 'Admin Userson',
    time_zone: 'UTC',
    password: 'changeme'
  )
  admin.confirm
  admin.roles << Role.find_by(name: :root)
  admin
end

def generate_user
  user = User.create(
    email: Faker::Internet.unique.email,
    username: Faker::Internet.unique.username,
    fullname: Faker::Name.name,
    country: Faker::Address.country_code,
    time_zone: Faker::Address.time_zone.split('/').last,
    password: 'changeme'
  )

  user.facebook = user.username if rand(10) > 3
  user.twitter = user.username if rand(10) > 4
  user.linkedin = user.username if rand(10) > 6
  user.parler = user.username if rand(10) > 8
  user.rumble = user.username if rand(10) > 9
  user.confirm if rand(10) > 3

  user.save

  user
end

def generate_show
  name = Faker::Lorem.words.join(' ')
  show = Show.create(
    name: Faker::Internet.slug(words: name, glue: '-'),
    title: name.titleize
  )

  show.description = Faker::Lorem.sentence if rand(10) > 3
  generate_show_links show
  show.save

  (0..rand(10)).each do |_n|
    generate_episode(show)
  end

  show
end

def generate_show_links(show)
  show.facebook = show.name if rand(10) > 3
  show.twitter = show.name if rand(10) > 4
  show.parler = show.name if rand(10) > 8
  show.rumble = show.name if rand(10) > 9
  show.spotify = show.name if rand(10) > 2
  show.apple_podcast = show.name if rand(10) > 2
  show.website = Faker::Internet.url if rand(10) > 8
end

def generate_episode(show)
  name = Faker::Lorem.words.join(' ')
  episode = Episode.create(
    show: show,
    name: Faker::Internet.slug(words: name, glue: '-'),
    title: name.titleize
  )

  episode.description = Faker::Lorem.sentence if rand(10) > 3
  episode.save
  episode
end

def generate_person
  name = Faker::Name.name
  user = User.order(Arel.sql('RANDOM()')).first
  admin = User.first
  person = Person.create(name: name, created_by: user)
  person.generate_slug
  person.save!

  person.confirm(admin) if rand(10) > 3
  person.block(admin) if rand(10) > 9

  person
end

def generate_vote
  vote = Vote.create(value: 1)
  vote.user = User.order(Arel.sql('RANDOM()')).first
  vote.person = Person.order(Arel.sql('RANDOM()')).first
  vote.show = Show.order(Arel.sql('RANDOM()')).first
  vote.save!
  vote
end
