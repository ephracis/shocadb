class CreateAliases < ActiveRecord::Migration[7.0]
  def change
    create_table :aliases do |t|
      t.string :name
      t.references :aliasable, polymorphic: true, null: false

      t.timestamps
    end
  end
end
