class AddFilterToSources < ActiveRecord::Migration[6.1]
  def change
    add_column :sources, :filter_pattern, :string
    add_column :sources, :title_pattern, :string
    add_column :sources, :youtube_kind, :string, default: 'channel'
  end
end
