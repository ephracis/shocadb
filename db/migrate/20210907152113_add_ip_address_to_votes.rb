class AddIpAddressToVotes < ActiveRecord::Migration[6.1]
  def change
    add_column :application_settings, :max_votes_per_ip, :integer, default: 1
    add_column :votes, :ip_address, :string, null: true
    change_column_null :votes, :user_id, true
  end
end
