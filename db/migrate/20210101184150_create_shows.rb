class CreateShows < ActiveRecord::Migration[6.1]
  def change
    create_table :shows do |t|
      t.string :name
      t.string :title
      t.string :youtube
      t.string :twitter
      t.string :facebook
      t.string :rumble
      t.string :parler
      t.string :description

      t.timestamps
    end
  end
end
