class AddEmailSettingsToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :email_notifications, :boolean, default: true
  end
end
