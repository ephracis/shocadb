class RemoveOldImageFromPeople < ActiveRecord::Migration[6.1]
  def change
    remove_column :people, :old_image
    remove_column :shows, :old_image
    remove_column :episodes, :old_image
  end
end
