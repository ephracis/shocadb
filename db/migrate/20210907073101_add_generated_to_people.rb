class AddGeneratedToPeople < ActiveRecord::Migration[6.1]
  def change
    add_column :episodes, :generated, :boolean, default: false
    add_column :people, :generated, :boolean, default: false
    add_column :sources, :guests_pattern, :string, default: 'w/(?<names>.*)'
    add_column :sources, :split_pattern, :string, default: 'and|&'
    add_column :sources, :parse_title_for_guests, :boolean, default: true
  end
end
