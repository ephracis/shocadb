class ChangeNameToSlug < ActiveRecord::Migration[6.1]
  def change
    rename_column :episodes, :name, :slug
    rename_column :shows, :name, :slug
  end
end
